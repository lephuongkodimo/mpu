################################################################################
# showdata.py
#
# Display analog data from Arduino using Python (matplotlib)
# 
# electronut.in
#
################################################################################

import sys, serial
import numpy as np
from time import sleep
from collections import deque
from matplotlib import pyplot as plt
import re
# class that holds analog data for N samples
class AnalogData:
  # constr
  def __init__(self, maxLen):
    self.ax = deque([0.0]*maxLen)
    self.maxLen = maxLen

  # ring buffer
  def addToBuf(self, buf, val):
    if len(buf) < self.maxLen:
      buf.append(val)
    else:
      buf.pop()
      buf.appendleft(val)

  # add data
  def add(self, data):
    assert(len(data) == 1)
    self.addToBuf(self.ax, data[0])
    
# plot class
class AnalogPlot:
  # constr
  def __init__(self, analogData):
    # set plot to animated
    plt.ion() 
    self.axline, = plt.plot(analogData.ax)
    plt.ylim([0, 400])

  # update plot
  def update(self, analogData):
    self.axline.set_ydata(analogData.ax)
    plt.draw()
def parse_data(tripa):
    # """Parse a string from the serial port
    # #parse_data('c00000000001i00000000001t22.22222p33.33333s00001e.')
    # #(1, 1, 22.22222, 33.33333, 1)
    # """
    _, value1, value2, value3,value4,value5,value6 = re.split('x|y|z|kx|ky|kz', tripa)
    value1 = float(value1)
    value2 = float(value2)
    value3 = float(value3)
    value4 = float(value4)
    value5 = float(value5)
    value6 = float(value6)
    return value1,value2,value3,value4,value5,value6
# main() function
def main():
  # expects 1 arg - serial port string
  if len(sys.argv) < 2:
    port = 'COM15'
  else:
    port = sys.argv[1]

  # plot parameters
  analogData = AnalogData(100)
  analogPlot = AnalogPlot(analogData)

  print ('plotting data...')

  # open serial port
  ser = serial.Serial(port, 115200)
  while True:
    try:
      line = ser.readline()
      try:
        value1, value2, value3,value4, value5, value6 = parse_data(line.decode('utf-8'))
        print (value1)
        analogData.add(value1)
        analogPlot.update(analogData)
      except:
        # skip line in case serial data is corrupt
        print("Invalid! cannot cast")
    except KeyboardInterrupt:
      print ('exiting')
      break
  # close serial
  ser.flush()
  ser.close()

# call main
if __name__ == '__main__':
  main()
