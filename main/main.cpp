/*
 * realtime.cpp
 *
 *  Created on: Nov 23, 2018
 *      Author: Admin
 */

/*
 * _main.cpp
 *
 *  Created on: Nov 15, 2018
 *      Author: Admin
 */

/*
 * this code for not realtime pla swap
 */

#include <esp_log.h>
#include <string>
#include "sdkconfig.h"
#include <stdio.h>
#include <cstdio>
#include <iostream>   // std::cout
#include  <MPU6050.h>
#include "Kalman.h"
#include "GSbutton.h"
#include "driver/gpio.h"
//#include "MPU6050_tockn.h"
#include <MatrixMath.h>
#include "MadgwickAHRS.h"
#include "MahonyAHRS.h"
#include "quaternion.h"
//#include "GSBLEconnet.h"
#include "Arduino.h"
#include "filter.h"
#include <sstream>
#include "Filters.h"
#include <BLEDevice.h>

float testFrequency = 0.1;                     // test signal frequency (Hz)
// The remote service we wish to connect to.
static BLEUUID serviceUUID("4fafc201-1fb5-459e-8fcc-c5c9c331914b");
// The characteristic of the remote service we are interested in.
static BLEUUID charUUID("beb5483e-36e1-4688-b7f5-ea07361b26a8");

static BLEAddress *pServerAddress;
static boolean doConnect = false;
static boolean connected = false;
static BLERemoteCharacteristic* pRemoteCharacteristic;

bool deviceConnected = false;
static int taskCore_r = 1;
static int taskCore_s = 0;
static bool onTaskShow = false;
uint8_t value = 0;
float dt;
static char tag[] = "MPU6050";
//*******************************
Madgwick filter_esp;
//Mahony filter_esp;
const uint8_t SCALE_GRAPH = 10;
//#define TEST_TIME_HOLD
#define GRAPH
//#define DIV // chia nho de realtime
#define REALTIME
//#define HIGH_PASS
//#define TESTING
//#define PRINT_GYR_ACC // OK
//#define PRINT_QANT  //
//#define PRINT_RRest
//#define PRINT_LINACC //linAcc -> linVel OK //adrui ok
//#define PRINT_TCACC//nghi ngho RR->tcAcc fail // aadruino ok ts
#define PRINT_LINVEL
//#define PRINT_LINVELHP //linVel to linVelHP tested
//#define PRINT_LINPOS
//#define PRINT_LINPOSHP //adruino pass
#ifdef TESTING
#include "dataorgin.h"
#endif
#define N 256
const int DONE_RICIVE_DATA_BIT = BIT0;
const int PRESS_BIT = BIT1;
static EventGroupHandle_t imu_event_group;
static unsigned long microsPrevious;
static unsigned long timeDelta;

const unsigned long microsPerReading = 3906; //100000/256
void convet(float RR[3][3], float R[9]);
void Multiply(mtx_type_f* A, mtx_type_f* B, int m, int p, int n, mtx_type_f* C);
bool connectToServer(BLEAddress pAddress);
void initBLE();
void firFloat( double *coeffs, double *input, double *output,    int filterLength );
/*
 * filter
 */
#define M 2
#define FILTER_LEN  M
static double coeffs[ FILTER_LEN ] =
{
		1,
		-0.997548637782850,
};


// maximum number of inputs that can be handled
// in one function call
#define MAX_INPUT_LEN   M
// maximum length of filter than can be handled
#define MAX_FLT_LEN     M //length of filter
#define BUFFER_LEN      (MAX_FLT_LEN - 1 + MAX_INPUT_LEN)
double insamp[BUFFER_LEN];
double buff_linVel[N];
double buff_linPos[N];

const vectord b_coeff = { { 0.998774318891425 }, { -0.998774318891425 }, };
const vectord a_coeff = { { 1.0 }, { -0.997548637782850 }, };

vectord y_filter_out;
const float samplePeriod[1] = { 0.00390625f };
const double samplePeriod_d[1] = { 0.004 };
const float Earth[3] = { 0.0f, 0.0f, 1.0f };
const float g[1] = { 9.81f };
const double mscale[1] = { 1.0 };

int fc = 0;
int count = 0;
unsigned long microsStart;
unsigned long microsEnd;
unsigned long delta;
const float gyroScale = 0.0175f;  //covet deg/s to radian/s for mahony

static float linVel[N][3] = { 0.0f, 0.0f, 0.0f };
static float linVelPre[3] = {0.0f, 0.0f, 0.0f };

static double linPos[N][3] = { 0.0f, 0.0f, 0.0f };
static double linPosPre[3]=  { 0.0f, 0.0f, 0.0f };
// array to hold input samples

//HP vel
static double linVelHP[3] ;

//HP pos
static double linPosHP[3];

float gtx, gty, gtz;  //deg/s
float atx, aty, atz;  //g
float Acc[3];
static float tcAcc[3];
static float linAcc[3];

float q[4];
float R[9];
float RR[3][3];
static double floatOutput[3];

float filteredArray[3];
static vectord HPx;
static vectord HPy;
static vectord HPz;
//*================================
static MPU6050 mpu6050;
//MPU6050 mpu6050(Wire);
static GSButtonInterup m_bt0;
static xQueueHandle gpio_evt_queue = NULL;
extern "C" {
void app_main(void);
}
// return the current time
float time() {
  return float( micros() ) * 1e-6;
}
/**
 * Scan for BLE servers and find the first one that advertises the service we are looking for.
 */
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
	/**
	 * Called for each advertising BLE server.
	 */
	void onResult(BLEAdvertisedDevice advertisedDevice) {
		Serial.print("BLE Advertised Device found: ");
		Serial.println(advertisedDevice.toString().c_str());

		// We have found a device, let us now see if it contains the service we are looking for.
		if (advertisedDevice.haveServiceUUID()
				&& advertisedDevice.getServiceUUID().equals(serviceUUID)) {

			//
			Serial.print("Found our device!  address: ");
			advertisedDevice.getScan()->stop();

			pServerAddress = new BLEAddress(advertisedDevice.getAddress());
			doConnect = true;

		} // Found our server
	} // onResult
};
// MyAdvertisedDeviceCallbacks

//static void IRAM_ATTR gpio_isr_handler(void* arg) {
//	uint32_t gpio_num = (uint32_t) arg;
////	m_bt0.bt->timerStart(TIMER_1);
//	xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
//
//}
//static void bt_task(void *param) {
//	gpio_evt_queue = xQueueCreate(100, sizeof(short)); //interup button
//	m_bt0.initGSInterup(GPIO_NUM_0, gpio_isr_handler, GPIO_INTR_LOW_LEVEL, 0); //interup button
//	short gyrDataIR[3];
//	uint32_t io_num;
//	while (1) {
//		if (xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {
//
////		    printf("x%.3fy%.3fz%.3fkx%.3fky%.3fkz%.3f\n",gyro_x,gyro_y,gyro_z,kfgyro_x ,kfgyro_y,kfgyro_z);
//		}
//	}
//
//}

static void mpu_task(void* param) {
	String taskMessage = "IMU running on core ";
	taskMessage = taskMessage + xPortGetCoreID();
	Serial.println(taskMessage);
	mpu6050.init(GPIO_NUM_21, GPIO_NUM_22);
	//SDA SCL
	mpu6050.calcGyroOffsets(true);
//	mmpu.setupInterup(); //INTERUP PIN -> PIN 4
//
//	Kalman myFilterGX(0.125, 32, 1023, 0);
//	Kalman myFilterGY(0.125, 32, 1023, 0);
//	Kalman myFilterGZ(0.125, 32, 1023, 0);
//	Kalman myFilterAX(0.125, 32, 1023, 0);
//	Kalman myFilterAY(0.125, 32, 1023, 0);
//	Kalman myFilterAZ(0.125, 32, 1023, 0);

	FilterOnePole filterOneHighpassVX( HIGHPASS, testFrequency );  // create a one pole (RC) highpass filter
	FilterOnePole filterOneHighpassVY( HIGHPASS, testFrequency );  // create a one pole (RC) highpass filter
	FilterOnePole filterOneHighpassVZ( HIGHPASS, testFrequency );  // create a one pole (RC) highpass filter

	FilterOnePole filterOneHighpassPX( HIGHPASS, testFrequency );  // create a one pole (RC) highpass filter
	FilterOnePole filterOneHighpassPY( HIGHPASS, testFrequency );  // create a one pole (RC) highpass filter
	FilterOnePole filterOneHighpassPZ( HIGHPASS, testFrequency );  // create a one pole (RC) highpass filter
//	float pitch = 0.0;
//	float roll = 0.0;
//	float *fpitch = &pitch;
//	float *froll = &roll;
	/*
	 * button
	 */
	gpio_pad_select_gpio(GPIO_NUM_0);
	gpio_set_direction(GPIO_NUM_0, GPIO_MODE_INPUT);
	gpio_set_pull_mode(GPIO_NUM_0, GPIO_PULLUP_ONLY);
	bool pressed = false;
	bool showResust = false;
	bool firt =  true;
	microsPrevious = micros();

	while (1)/*
	 * startlop
	 */{
		unsigned long microsNow;
//press->t=1/512->read imu->store all time
// check if it's time to read data and update the filter
		/*
		 * press read and store data
		 */
// while(z < N){
		if (digitalRead(GPIO_NUM_0) == HIGH) {
			xEventGroupSetBits(imu_event_group, PRESS_BIT);
//			resetIMU_TO_ZERO();
			if (!pressed) {
				microsPrevious = micros();
				pressed = true; //khi ko nhan press false
				count = 1; //when not press cout = 0
			}

			microsNow = micros();
			/*
			 * for deltal time
			 */
#ifdef TEST_TIME_HOLD
			if (count == 0) {
				microsStart = microsNow;
			}
#endif
			/*
			 * if time loop >= 1/256s imu update
			 */
			if (microsNow - microsPrevious >= microsPerReading) {
				timeDelta=microsNow - microsPrevious;
				mpu6050.readGyro();
				float gyroX = mpu6050.getGyroX();
				float gyroY = mpu6050.getGyroY();
				float gyroZ = mpu6050.getGyroZ();

				mpu6050.readAccel();
				float accelX = mpu6050.getAccX();
				float accelY = mpu6050.getAccY();
				float accelZ = mpu6050.getAccZ();

				gtx = gyroX;
				gty = gyroY;
				gtz = gyroZ;        //deg/s
				atx = accelX;
				aty = accelY;        //g
				atz = accelZ;
#ifdef PRINT_GYR_ACC
#ifdef GRAPH
				Serial.printf("%0.4f %0.4f %0.4f %0.4f %0.4f %0.4f\r\n",
						atx, aty, atz, gtx,
						gty, gtz);
#else
				Serial.printf("count=%d\n",count);
				Serial.printf("ax: %0.4f ay: %0.4f az:%0.4f\n",atx[count],aty[count],atz[count]); //Serial.println("");
				Serial.printf("gx: %0.4f gy: %0.4f gz:%0.4f\n",gtx[count],gty[count],gtz[count]);//Serial.println("");
#endif
#endif
				/*
				 end for realtime
				 */
//				if(firt){
//
//				} else if(count%8==0)
//				{
//				Serial.printf("%.4f %.4f %.4f\r\n",SCALE_GRAPH*HPx[count], SCALE_GRAPH*HPy[count], SCALE_GRAPH*HPz[count]);
//				}

			 //if detal >=microsPerReading

			gtx = gtx * gyroScale;
			gty = gty * gyroScale;
			gtz = gtz * gyroScale;
			filter_esp.updateIMU(gtx, gty, gtz, atx, aty, atz);
			filter_esp.getQuat(q);
			Quaternion<float> mQuat(q[0], -q[1], -q[2], -q[3]);
			mQuat.Quaternion2Matrix(R);
			convet(RR, R);
			//  %% Calculate 'tilt-compensated' accelerometer

			Acc[0] = atx;
			Acc[1] = aty;
			Acc[2] = atz;
			Matrix.Multiply((mtx_type_f*) RR, (mtx_type_f*) Acc, 3, 3, 1,
					(mtx_type_f*) tcAcc);
#ifdef PRINT_TCACC
#ifdef GRAPH
				Serial.printf("%0.4f %0.4f %0.4f\r\n", tcAcc[0],	tcAcc[1], tcAcc[2]);
#else
#endif
#endif

			////%% Calculate linear acceleration in Earth frame (subtracting gravity)

			Matrix.Subtract((mtx_type_f*) tcAcc, (mtx_type_f*) Earth, 3, 1,
					(mtx_type_f*) linAcc);
			float tempLinAcc[3];
			Matrix.Multiply((mtx_type_f*) linAcc, (mtx_type_f*) g, 3, 1, 1,
					(mtx_type_f*) tempLinAcc);
			Matrix.Copy((mtx_type_f*) tempLinAcc, 3, 1, (mtx_type_f*) linAcc);

			//%% Calculate linear velocity (integrate acceleartion)

			float tempMultiVel[3];
			Matrix.Multiply((mtx_type_f*) linAcc, (mtx_type_f*) samplePeriod,
					3, 1, 1, (mtx_type_f*) tempMultiVel);
			float tempAddVel[3];
			Matrix.Add((mtx_type_f*) linVelPre, (mtx_type_f*) tempMultiVel,
					3, 1, (mtx_type_f*) tempAddVel);
			Matrix.Copy((mtx_type_f*) tempAddVel, 3, 1, (mtx_type_f*) linVel[count]);
			Matrix.Copy((mtx_type_f*) linVel[count], 3, 1, (mtx_type_f*) linVelPre);
#ifdef PRINT_LINVEL
#ifdef GRAPH
//				Serial.printf("count:%d LinVel:%0.4f %0.4f %0.4f\r\n", count,linVel[count][0],linVel[count][1], linVel[count][2]);
#else
#endif
#endif

			//    %% High-pass filter linear velocity to remove drift
			// perform the filtering
			/*
			 * input minhf phiai nghien cuu lay sao cho duoc M mau
			 *
			 */

//			double floatInput[3][FILTER_LEN]={
//					// --------M-i--------------------M--------//
//					{(double)(linVel[count-1][0]),(double)(linVel[count][0])}, //x
//					{(double)(linVel[count-1][1]),(double)(linVel[count][1])}, //y
//					{(double)(linVel[count-1][2]),(double)(linVel[count][2])}, //x
//						};

//	//		Serial.printf(">>floatInput: %0.4f floatOutput:%0.4f\r\n",floatInput[0][0],floatOutput[0]);
//			firFloat( coeffs, floatInput[0], &(floatOutput[0]), FILTER_LEN);
//			firFloat( coeffs, floatInput[1], &(floatOutput[1]), FILTER_LEN);
//			firFloat( coeffs, floatInput[2], &(floatOutput[2]), FILTER_LEN);
			filterOneHighpassVX.input( linVel[count][0] );
			filterOneHighpassVY.input( linVel[count][1] );
			filterOneHighpassVZ.input( linVel[count][2] );
			linVelHP[0]= (double)filterOneHighpassVX.output();
			linVelHP[1]= (double)filterOneHighpassVY.output();
			linVelHP[2]= (double)filterOneHighpassVZ.output();


//			Serial.printf(">>>floatOutput1: %0.4f\r\n", floatOutput[0]);
//			Matrix.Multiply((mtx_type_d*)floatOutput, (mtx_type_d*)mscale, 3, 1, 1, (mtx_type_d*)linVelHP);
//			Matrix.Copy((mtx_type_d*)floatOutput, 3, 1, (mtx_type_d*)linVelHP);
//			Serial.printf(">>>linVelHP: %0.4f\r\n", linVelHP[0]);

#ifdef PRINT_LINVELHP
#ifdef GRAPH
				Serial.printf("%0.4f %0.4f %0.4f\r\n", linVelHP[0],	linVelHP[1], linVelHP[2]);
#else
#endif
#endif
			//%% Calculate linear possion (integrate vel)
			double tempMultiPos[3];
			Matrix.Multiply((mtx_type_d*)linVelHP, (mtx_type_d*)samplePeriod_d, 3, 1, 1, (mtx_type_d*)tempMultiPos);
			double tempAddPos[3];
			Matrix.Add((mtx_type_d*) linPosPre, (mtx_type_d*) tempMultiPos, 3,1, (mtx_type_d*) tempAddPos);
//			Serial.printf(">>>>>1 tempAddPos: %0.6f\r\n",  tempAddPos[0]);

			Matrix.Copy((mtx_type_d*)tempAddPos, 3, 1, (mtx_type_d*)linPos[count]);
//			Serial.printf(">>>>2 linPos: %0.6f\r\n",  linPos[count][0]);

			Matrix.Copy((mtx_type_d*)linPos[count], 3, 1, (mtx_type_d*)linPosPre);
#ifdef PRINT_LINPOS
#ifdef GRAPH
//				Serial.printf("%0.4f %0.4f %0.4f\r\n", linPos[count][0],	linPos[count][1], linPos[count][2]);
#else
#endif
#endif
			////%% High-pass filter linear pos to remove drift
//			Serial.printf(">>>>> linPos: %0.6f\r\n",  linPos[count][0]);

//			double floatInput_[3][FILTER_LEN]={
//					// --------M-i--------------------M--------//
//					{	(double)(linPos[count-1][0]),(double)(linPos[count][0])}, //x
//					{	(double)(linPos[count-1][1]),(double)(linPos[count][1])}, //y
//					{	(double)(linPos[count-1][2]),(double)(linPos[count][2])}, //x
//									};
////			Serial.printf(">>>>> floatInput: %0.6f\r\n",  floatInput_[0][1]);
//
//			firFloat( coeffs, floatInput_[0], &(floatOutput[0]),FILTER_LEN);
//			firFloat( coeffs, floatInput_[1], &(floatOutput[1]),FILTER_LEN);
//			firFloat( coeffs, floatInput_[2], &(floatOutput[2]), FILTER_LEN);

			filterOneHighpassPX.input( (float)linPos[count][0] );
			filterOneHighpassPY.input( (float)linPos[count][1] );
			filterOneHighpassPZ.input( (float)linPos[count][2] );

			linPosHP[0]= (double)filterOneHighpassPX.output();
			linPosHP[1]= (double)filterOneHighpassPY.output();
			linPosHP[2]= (double)filterOneHighpassPZ.output();


////			Serial.printf(">>>>>4floatOutput: %0.4f\r\n", 1000*floatOutput[0]);
//			Matrix.Copy((mtx_type_d*)floatOutput, 3, 1, (mtx_type_d*)linPosHP);
#ifdef PRINT_LINPOSHP
#ifdef GRAPH
				Serial.printf("%0.4f %0.4f %0.4f\r\n", linPosHP[0],	linPosHP[1], linPosHP[2]);
#else
#endif
#endif
			count++;
			microsPrevious = microsPrevious + microsPerReading;
	if(		count%5 == 0 ) xEventGroupSetBits(imu_event_group, DONE_RICIVE_DATA_BIT);
}//>
			if(count==N){
				Matrix.Copy((mtx_type_d*)linPos[count -1], 3, 1, (mtx_type_d*)linPos[0]);
				Matrix.Copy((mtx_type_f*)linVel[count -1], 3, 1, (mtx_type_f*)linVel[0]);
				count = 1;
			}

		}  //press
		else {
			pressed = false;
			vTaskDelay(50 / portTICK_PERIOD_MS);

		}

/*
 * end lop
 */
}
}
void showTask(void*param){
	String taskMessage = "Show Task running on core ";
	taskMessage = taskMessage + xPortGetCoreID();
	Serial.println(taskMessage);
	while(1){

		/*
		 * press va reaad all data pass
		 * press: clear adn read again
		 */
	EventBits_t uxBits;
	uxBits = xEventGroupWaitBits(imu_event_group,
				DONE_RICIVE_DATA_BIT | PRESS_BIT, true, true, (TickType_t) 1);
	if ((uxBits & (PRESS_BIT | DONE_RICIVE_DATA_BIT))
				== (PRESS_BIT | DONE_RICIVE_DATA_BIT)) {
//		Serial.printf("%0.4f %0.4f %0.4f %0.4f %0.4f %0.4f\r\n",linVel[count][0],linVel[count][1],linVel[count][2],linVelHP[0],	linVelHP[1], linVelHP[2]);
	Serial.printf("%0.6f %0.6f %0.6f %0.6f %0.6f %0.6f\r\n",linAcc[0], linAcc[1], linAcc[2],Acc[0],Acc[1],Acc[2]);
// realtime
	} else {

	}
	}
}
void app_main(void) {
Serial.begin(115200);
Serial.println("Starting Arduino BLE Client application...");
initBLE();
filter_esp.begin(256.0);
imu_event_group = xEventGroupCreate();
xTaskCreatePinnedToCore(&mpu_task, "mpu_task", 20000, NULL, 2, NULL,taskCore_r);
xTaskCreatePinnedToCore(&showTask, "show", 2048, NULL, 2, NULL,taskCore_s);
}
void initBLE() {
Serial.println("Starting Arduino BLE Client application...");
BLEDevice::init("");
// Retrieve a Scanner and set the callback we want to use to be informed when we
// have detected a new device.  Specify that we want active scanning and start the
// scan to run for 30 seconds.
BLEScan* pBLEScan = BLEDevice::getScan();
pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
pBLEScan->setActiveScan(true);
pBLEScan->start(5);
}
void convet(float RR[3][3], float R[9]) {
int i, j;
for (i = 0; i < 3; ++i)
for (j = 0; j < 3; ++j) {
	RR[i][j] = R[i * 3 + j];
}
}
void Multiply(mtx_type_f* A, mtx_type_f* B, int m, int p, int n,
mtx_type_f* C) {
	  // A = input matrix (m x p)
	  // B = input matrix (p x n)
	  // m = number of rows in A
	  // p = number of columns in A = number of rows in B
	  // n = number of columns in B
	  // C = output matrix = A*B (m x n)
int i, j, k;
for (i = 0; i < m; i++) {
for (j = 0; j < n; j++) {
	C[n * i + j] = 0;
	for (k = 0; k < p; k++)
		C[n * i + j] = C[n * i + j] + A[p * i + k] * B[n * k + j];
}
}
}

bool connectToServer(BLEAddress pAddress) {
//    Serial.print("Forming a connection to ");
//    Serial.println(pAddress.toString().c_str());

BLEClient* pClient = BLEDevice::createClient();
Serial.println(" - Created client");

	// Connect to the remove BLE Server.
pClient->connect(pAddress);
Serial.println(" - Connected to server");

	// Obtain a reference to the service we are after in the remote BLE server.
BLERemoteService* pRemoteService = pClient->getService(serviceUUID);
if (pRemoteService == nullptr) {
//      Serial.print("Failed to find our service UUID: ");
//      Serial.println(serviceUUID.toString().c_str());
return false;
}
Serial.println(" - Found our service");

// Obtain a reference to the characteristic in the service of the remote BLE server.
pRemoteCharacteristic = pRemoteService->getCharacteristic(charUUID);
if (pRemoteCharacteristic == nullptr) {
//      Serial.print("Failed to find our characteristic UUID: ");
//      Serial.println(charUUID.toString().c_str());
return false;
}
Serial.println(" - Found our characteristic");

return true;
}

// the FIR filter function
void firFloat( double *coeffs, double *input, double *output, int filterLength )
{
    double acc;     // accumulator for MACs
    double *coeffp; // pointer to coefficients
    double *inputp; // pointer to input samples
    int n;
    int k;

    // put the new samples at the high end of the buffer
    memcpy( &insamp[filterLength - 1], input,
    		filterLength * sizeof(double) );

    // apply the filter to each input sample
        // calculate output n
        coeffp = coeffs;
        inputp = &insamp[filterLength - 1 + filterLength - 1 ];
        acc = 0;
        for ( k = 0; k < filterLength; k++ ) {
//        	Serial.printf(">>>coeffp: %0.4f inputp:%0.4f\r\n",*coeffp,*inputp);
            acc += (*coeffp++) * (*inputp--);
//            Serial.printf("coeffp: %0.4f inputp:%0.4f\r\n",*coeffp,*inputp);
//            Serial.printf("acc %0.4f\r\n",acc);
        }
        *output = acc;
//        Serial.printf("<<<<output:%0.4f\r\n",*output);



}

