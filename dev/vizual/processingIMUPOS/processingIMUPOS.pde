import processing.serial.*;
Serial myPort;
float center = 500.0;
float X = 0.0;
float Y = 0.0;
float Z = 0.0;
float xp =center , yp= center;
boolean onClean = false;
int c = 0;
void setup()
{
  //size(600, 500, P3D);
  size(1000, 1000,P2D );
  background(255);
  translate(10, height-10);
  scale(1, -1);
  stroke(0);
  strokeWeight(4);
  line(0, 0, 1000, 0);
  line(0, 0, 0, 1000);
  stroke(255,0,0);
  // if you have only ONE serial port active
  //myPort = new Serial(this, Serial.list()[0], 115200); // if you have only ONE serial port active

  // if you know the serial port name
  myPort = new Serial(this, "COM3", 115200);                    // Windows
  //myPort = new Serial(this, "/dev/ttyACM0", 9600);             // Linux
  //myPort = new Serial(this, "/dev/cu.usbmodem1217321", 9600);  // Mac

  textSize(16); // set text size
  textMode(SHAPE); // set text mode to shape
   //initDraw();
}

void draw()
{
  serialEvent();  // read and parse incoming serial message
  //background(255); // set background to white

  //drawArduino();
  //popMatrix(); // end of object
  if(onClean){
    background(255); // set background to white
    onClean= false;
  }
  drawNow(X,Y,10.0);

   //Print values to console
  //print(pitch);
  //print("\t");
  //print(roll);
  //print("\t");
  //print(yaw);
  //println();
}

void serialEvent()
{
  int newLine = 13; // new line character in ASCII
  String message;
  do {
    message = myPort.readStringUntil(newLine); // read from port until new line
    if (message != null) {
      String[] list = split(trim(message), " ");
      if (list.length >= 4 && list[0].equals("pos:")) {
        X = float(list[1]); // convert to float yaw
        Y = float(list[2]); // convert to float pitch
        Z = float(list[3]); // convert to float roll
        c++;
      } 
      if (list[0].equals("Clean")) {
        c=0;
        //onClean = true;
      }
    }
  } while (message != null);
}

void drawArduino()
{
  /* function contains shape(s) that are rotated with the IMU */
  stroke(0, 90, 90); // set outline colour to darker teal
  fill(0, 130, 130); // set fill colour to lighter teal
  box(300, 10, 200); // draw Arduino board base shape

  stroke(0); // set outline colour to black
  fill(80); // set fill colour to dark grey

  translate(60, -10, 90); // set position to edge of Arduino box
  box(170, 20, 10); // draw pin header as box

  translate(-20, 0, -180); // set position to other edge of Arduino box
  box(210, 20, 10); // draw other pin header as box
}

void drawNow(float XX,float YY,float R){//deg
float x = R*XX+center;
float y = R*YY+center;
ArrayList<PVector> al = new ArrayList();
print("x : y");
print(x);
print("\t");
print(y); 
println();
al.add( new PVector(xp,yp));
al.add( new PVector(x,y));
if (c<10){
} else{
line(al.get(0).x,al.get(0).y,al.get(1).x,al.get(1).y);
}
xp =x;
yp =y;
}
void initDraw(){
size(220, 220);
background(255);
translate(10, height-10);
scale(1, -1);
stroke(0);
line(0, 0, 2, 0);
line(0, 0, 0, 2);
stroke(255,0,0);
}
