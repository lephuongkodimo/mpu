import processing.serial.*;
Serial myPort;

float yaw = 0.0;
float pitch = 0.0;
float roll = 0.0;
float yawH = 0.0;
float pitchH = 0.0;
float rollH = 0.0;
void setup()
{
  size(800, 800, P3D);

  // if you have only ONE serial port active
  //myPort = new Serial(this, Serial.list()[0], 9600); // if you have only ONE serial port active

  // if you know the serial port name
  myPort = new Serial(this, "COM3", 115200);                    // Windows
  //myPort = new Serial(this, "/dev/ttyACM0", 9600);             // Linux
  //myPort = new Serial(this, "/dev/cu.usbmodem1217321", 9600);  // Mac

  textSize(16); // set text size
  textMode(SHAPE); // set text mode to shape
}

void draw()
{
  serialEvent();  // read and parse incoming serial message
  background(255); // set background to white
  lights();

  translate(width/2, height/2); // set position to centre

  pushMatrix(); // begin object
  float c1 = cos(radians(roll));
  float s1 = sin(radians(roll));
  float c2 = cos(radians(pitch));
  float s2 = sin(radians(pitch));
  float c3 = cos(radians(yaw));
  float s3 = sin(radians(yaw));
  applyMatrix( c2*c3, s1*s3+c1*c3*s2, c3*s1*s2-c1*s3, 0,
               -s2, c1*c2, c2*s1, 0,
               c2*s3, c1*s2*s3-c3*s1, c1*c3+s1*s2*s3, 0,
               0, 0, 0, 1);
  drawArduino();
  popMatrix(); // end of object
  
  pushMatrix(); // begin object
  translate(0, 200);
  float c1H = cos(radians(rollH));
  float s1H = sin(radians(rollH));
  float c2H = cos(radians(pitchH));
  float s2H = sin(radians(pitchH));
  float c3H = cos(radians(yawH));
  float s3H = sin(radians(yawH));
  applyMatrix( c2H*c3H, s1H*s3H+c1H*c3H*s2H, c3H*s1H*s2H-c1H*s3H, 0,
               -s2H, c1H*c2H, c2H*s1H, 0,
               c2H*s3H, c1H*s2H*s3H-c3H*s1H, c1H*c3H+s1H*s2H*s3H, 0,
               0, 0, 0, 1);
  drawArduino();
  popMatrix(); // end of object

  // Print values to console
  print(roll);
  print("\t");
  print(pitch);
  print("\t");
  print(yaw);
  print("\t");print("\t");print("\t");
  print(rollH);
  print("\t");
  print(pitchH);
  print("\t");
  print(yawH);
  println();
}

void serialEvent()
{
  int newLine = 13; // new line character in ASCII
  String message;
  do {
    message = myPort.readStringUntil(newLine); // read from port until new line
    if (message != null) {
      String[] list = split(trim(message), " ");
      if (list[0].equals("OK")) {
        print("-----------OK-------------");
      }
      if (list.length >= 4 && list[0].equals("Orientation:")) {
        yaw = float(list[1]); // convert to float yaw
        pitch = float(list[2]); // convert to float pitch
        roll = float(list[3]); // convert to float roll
      }
      if (list.length >= 4 && list[0].equals("OrientationH:")) {
        yawH = float(list[1]); // convert to float yaw
        pitchH = float(list[2]); // convert to float pitch
        rollH = float(list[3]); // convert to float roll
      }
    }
  } while (message != null);
}

void drawArduino()
{
  /* function contains shape(s) that are rotated with the IMU */
  stroke(0, 90, 90); // set outline colour to darker teal
  fill(0, 130, 130); // set fill colour to lighter teal
  box(300, 10, 200); // draw Arduino board base shape

  stroke(0); // set outline colour to black
  fill(80); // set fill colour to dark grey

  translate(60, -10, 90); // set position to edge of Arduino box
  box(170, 20, 10); // draw pin header as box

  translate(-20, 0, -180); // set position to other edge of Arduino box
  box(210, 20, 10); // draw other pin header as box
}
