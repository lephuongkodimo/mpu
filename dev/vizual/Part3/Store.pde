





class Store
{
  String addr ;
  int Row,Col;
  Table table;
  Store(String name,int num_nn, int num_w) {
    addr = "data/"+name+".csv";
    Col = num_nn;
    Row = num_w;
    table = new Table();
    for(int i=0 ;i < Col ; i++){
    table.addColumn("["+i+"]");    
      }
  }

  void save(Neuron [] rlayer){
    for(int i = 0; i < Row ; i++){
        TableRow newRow = table.addRow();
            for(int j =0 ;j < Col ;j++){
              String NameCol = "["+j+"]";
              newRow.setFloat(NameCol,rlayer[j].weights[i]);
    }
    }
  saveTable(table, addr);
  }
}
class reStore
{  
  String addr ;
  int Row,Col;
  Table table;
  reStore(String name,int num_nn, int num_w){
    addr = "data/"+name+".csv";
    Col = num_nn;
    Row = num_w;
    table = loadTable(addr, "header");
  }
  void load(Neuron [] rlayer){
    int j = 0;
       for (TableRow row : table.rows()) {
   for(int i = 0 ; i < table.getColumnCount() ; i ++   ){
     String NameCol = "["+i+"]";
     float value = row.getFloat(NameCol);
     rlayer[i].weights[j]=value;
   }
     j++;
  }
  }
}
