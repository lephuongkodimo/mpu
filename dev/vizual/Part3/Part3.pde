/*

 Charles Fried - 2017
 ANN Tutorial 
 Part #1
 
 MAIN TAB
 
 Original from Alasdair Turner (c) 2009
 Free software: you can redistribute this program and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 */


int totalTrain = 0;
int totalTest = 0;
int totalRight = 0;
float sucess = 0;
int testCard = 0;
int trainCard = 0;

Network neuralnet;
Button trainB, testB,loadB,detecWand;
reStore storeHidenLayer;
reStore storeOutputLayer;
void setup() {
  testByte();
  size(1000, 400);
  setupSigmoid();
  loadData();
  neuralnet = new Network(196, 49, 10);
  storeHidenLayer = new reStore("HidenLayer",49,196);
  storeOutputLayer = new reStore("OutPutLayer",10,49);
  //load weights
  
  smooth();
  stroke(150);
  
  trainB = new Button(width*0.06, height*0.9, "Train");
  testB = new Button(width*0.11, height*0.9, "Test");
  loadB = new Button(width*0.16, height*0.9, "Load");
  detecWand = new Button(width*0.21, height*0.9, "Wand");
}

void draw() {

  background(255);
  neuralnet.display();
  
  fill(100);
  text("Test card: #" + testCard, width*0.18, height*0.89);
  text("Train card: " + trainCard, width*0.18, height*0.93);
  
  text("Total train: " + totalTrain, width*0.32, height*0.89);
  text("Total test: " + totalTest, width*0.32, height*0.93);
  
  if(totalTest>0) sucess = float(totalRight)/float(totalTest);
  text("Success rate: " + nfc(sucess, 2), width*0.44, height*0.89);
  text("Card label: " + testing_set[testCard].output, width*0.44, height*0.93);
  
  trainB.display();
  testB.display();
  loadB.display();
  detecWand.display();
  
  loadWand();
  neuralnet.respond(ImageWand[0]);
  neuralnet.display();
}

void mousePressed() {
  if (trainB.hover()) {
    for (int i = 0; i < 2; i++) {
      trainCard = (int) floor(random(0, training_set.length));
      neuralnet.respond(training_set[trainCard]);
      neuralnet.train(training_set[trainCard].outputs);
      totalTrain++;
    }
  } else if (testB.hover()){
    testCard = (int) floor(random(0, testing_set.length));
    neuralnet.respond(testing_set[testCard]);
    //load weights
    
    neuralnet.display();
    if(neuralnet.bestIndex == testing_set[testCard].output) totalRight ++;
    println(testCard + "  " + neuralnet.bestIndex + "  " + testing_set[testCard].output);
    //storeHidenLayer.save(neuralnet.hidden_layer);
    //storeOutputLayer.save(neuralnet.output_layer);
    totalTest ++;
  } else if(loadB.hover()){
  storeHidenLayer.load(neuralnet.hidden_layer);
  storeOutputLayer.load(neuralnet.output_layer);
  } else if(detecWand.hover()){
    loadWand();
    neuralnet.respond(ImageWand[0]);
    neuralnet.display();
  }
    
  
  redraw();
}
