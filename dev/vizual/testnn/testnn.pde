import processing.serial.*;
Serial myPort;
float center = 500.0;
float XXpre = 0.0;
float YYpre = 0.0;
float XX = 0.0;
float YY = 0.0;
float XXX = 0.0;
float YYY = 0.0;
float X = 0.0;
float Y = 0.0;
float Z = 0.0;
float Distan = 0.0;
float Angle = 0.0;
float Anglet = 0.0;
float AnglePre = 0.0;
float totalDistan = 0.0;
float xp =center , yp= center;
float XXp =center , YYp= center;
float rAngle=0.0;
boolean onClean = false;
int c = 0;
PFont f;
String Distance=" ";
HLine h1 = new HLine(1.0, center); 
HLine h2 = new HLine(1.0, center); 
byte[] nums;
void setup()
{
  //size(600, 500, P3D);
  size(1000, 1000,P2D );
  background(255);
  translate(0, height);
  scale(1, -1);
  stroke(0);
  strokeWeight(1);
  line(0, center, 1000, center);
  line(center, 0, center, 1000);
  stroke(255,0,0);
  f = createFont("Arial",20,true);
  textFont(f,20);
  // if you have only ONE serial port active
  //myPort = new Serial(this, Serial.list()[0], 115200); // if you have only ONE serial port active

  nums = new byte[196];
  for(byte i =0 ; i < 196 ; i ++){
  nums[i] = i ;//0-255
  }
  saveBytes("wand.dat", nums);
  // if you know the serial port name
  myPort = new Serial(this, "COM3", 115200);                    // Windows



  textSize(16); // set text size
  textMode(SHAPE); // set text mode to shape
   //initDraw();
   
}

void draw()
{
  serialEvent();  // read and parse incoming serial message
  //background(255); // set background to white

  //drawArduino();
  //popMatrix(); // end of object
  if(onClean){
    background(255); // set background to white
    //translate(0, height);
    stroke(0);
    strokeWeight(1);
    line(0, center, 1000, center);
    line(center, 0, center, 1000);
    onClean= false;
  }
  
  strokeWeight(2);  // Default
  stroke(0, 102, 150);
  h1.update(X,-Y);
  

  strokeWeight(4);
  stroke(204, 102, 0);
  point(XX+center, -YY+center);
  
  
  //strokeWeight(7);
  //stroke(204, 0, 200);
  //point(-XXX+center, -YYY+center);
   //h1.update(-XX,-YY);
  //rAngle = rAngle +Angle;
  //XX = XX + Distan*sin(rAngle);
  //XX = YY + Distan*cos(rAngle);
   //noStroke();
   //fill(204,204,204);
   //rect(50,100-20,textWidth(Distance),20);// erase text

   //drawNow(X,Y,2.0);

  // pushMatrix();
  //drawPosNow(XX,YY,2.0);
  //popMatrix();

   //Print values to console
  print(Distan);
  print("\t");
  print(Angle);
  print("\t");
  //print(XX);
   //print("        \t");
    //print(YY);
     //print("\t");
      print(XX);
   print("\t");
    print(YY);
  println();
}

void serialEvent()
{
  int newLine = 13; // new line character in ASCII
  String message;
  do {
    message = myPort.readStringUntil(newLine); // read from port until new line
    if (message != null) {
      String[] list = split(trim(message), " ");
      print(message);
      if (list.length >= 2 && list[0].equals("Co:")) {
        X = float(list[1]); // convert to float x
        Y = float(list[2]); // convert to float y
        //Z = float(list[3]); // convert to float z
        
        c++;
      }   
      if (list.length >= 2 && list[0].equals("Po:")) {
        XXX = float(list[1]); // convert to float x
        YYY = float(list[2]); // convert to float y
        Distan = float(list[3]); // convert to float z
        Angle = float(list[4]); // convert to float z
        Anglet = float(list[5]); // convert to float z
        rAngle=Angle;//+AnglePre;
        AnglePre = rAngle;
        XX=XXpre+Distan*sin(radians(rAngle));
        YY=YYpre+Distan*cos(radians(rAngle));
        XXpre = XX;
        YYpre = YY;
        c++;
      } 
      if (list[0].equals("Clean")) {
        c=0;
        onClean = true;
      }
      if (list[0].equals("Connected")) {
        print("Connected");
      }
    }
  } while (message != null);
}

void drawArduino()
{
  /* function contains shape(s) that are rotated with the IMU */
  stroke(0, 90, 90); // set outline colour to darker teal
  fill(0, 130, 130); // set fill colour to lighter teal
  box(300, 10, 200); // draw Arduino board base shape

  stroke(0); // set outline colour to black
  fill(80); // set fill colour to dark grey

  translate(60, -10, 90); // set position to edge of Arduino box
  box(170, 20, 10); // draw pin header as box

  translate(-20, 0, -180); // set position to other edge of Arduino box
  box(210, 20, 10); // draw other pin header as box
}

void drawNow(float XX,float YY,float R){//deg
float x = -R*XX+center;
float y = -R*YY+center;
ArrayList<PVector> al = new ArrayList();
//print("x : y");
//print(x);
//print("\t");
//print(y); 
//println();
al.add( new PVector(xp,yp));
al.add( new PVector(x,y));
if (c<5){
} else{
line(al.get(0).x,al.get(0).y,al.get(1).x,al.get(1).y);
}
xp =x;
yp =y;


}
void drawPosNow(float XX,float YY,float R){//deg
float x = -R*XX+center;
float y = -R*YY+center;
ArrayList<PVector> all = new ArrayList();
print("XX :YY");
print(XX);
print("\t");
print(YY); 
println();
all.add( new PVector(XXp,YYp));
all.add( new PVector(x,y));
if (c<5){
} else{
Distance = "Distance:" + Distan + " ||Angle:" + Angle;
//fill(0);
//text(Distance,50,100+30*c);
rect(30, 20, 55, 55, 7);
stroke(50);
line(all.get(0).x,all.get(0).y,all.get(1).x,all.get(1).y);
//noStroke();
//fill(204,204,204);
//rect(50,100-20,textWidth(Distance),20);// erase text


}
XXp =x;
YYp =y;
}
void initDraw(){
size(220, 220);
background(255);
translate(10, height-10);
scale(1, -1);
stroke(0);
line(0, 0, 2, 0);
line(0, 0, 0, 2);
stroke(255,0,0);
}
class HLine { 
  float ypos=0, xpos =0;
  float R,Center;
  HLine (float r,float c  ) {
    R=r;
    Center = c;
  } 
  void update(float x, float y) { 
  line(xpos+Center, ypos+Center, x+Center, y+Center); 
        ypos = y; 
        xpos = x;
  } 
} 
