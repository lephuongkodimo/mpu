#include "MadgwickAHRS.h"
#include "MahonyAHRS.h"
#include "quaternion.h"
#include "MatrixMath.h"
#include <cstdio>
#include <iostream>
#include <vector>
#include "dataorgin.h"

using namespace std;
//Madgwick filter;
Mahony filter;
#define N 100

#define PRINT_GYR_ACC // OK
#define PRINT_QANT	// 
#define PRINT_RR
#define PRINT_TCACC//nghi ngho RR->tcAcc fail
#define PRINT_LINACC //linAcc -> linVel OK
#define PRINT_LINVEL
#define PRINT_LINVELHP //linVel to linVelHP tested
#define PRINT_LINPOS    
#define PRINT_LINPOSHP

int dot(float A[3][3], float B[3][1], float C[3][1]);
int convet(float RR[3][3], float R[9]);

float samplePeriod[1] = { 0.002f };
float Earth[3] = { 0.0f, 0.0f, 1.0f };
float g[1] = { 9.81f };
int fc = 0;

float linVel[N][3] = { 0.0f, 0.0f, 0.0f };
float linVelPre[3];
float linPos[N][3] = { 0.0f, 0.0f, 0.0f };
float linPosPre[3];

//HP vel
float mVelEMA_a[1] = { 0.7f };    //initialization of EMA alpha
float mVelOne[1] = { 1.0f };
float mVelEMA_S[3] = { 0.0f, 0.0f, 0.0f };        //initialization of EMA S
float linVelHP[N][3];

//HP pos

float mPosOne[1] = { 1.0f };
float linPosHP[N][3];

float gtx[N], gty[N], gtz[N];        //deg/s
float atx[N], aty[N], atz[N];        //g
bool XUAT = 0;
int main() {

	float Acc[N][3];
	float tcAcc[N][3];
	float linAcc[N][3];

	float q[N][4];
	float R[N][9];
	float RR[N][3][3];
	float qq[4]={0.9239, 0,0.3827,0
	};
	float RRR[9];
	float RRRR[3][3];
	printf("================================================================\n\n");
	
	
	Quaternion<float> mQuat(qq[0], -qq[1], -qq[2], -qq[3]);
	mQuat.Quaternion2Matrix(RRR);
	convet(RRRR, RRR);
	Matrix.Print((mtx_type*) RRRR, 3, 3);
//		for (int i = 0; i < N; i++) {
//		gtx[i] = Data[i][0];
//		gty[i] = Data[i][1];
//		gtz[i] = Data[i][2];
//		atx[i] = Data[i][3];
//		aty[i] = Data[i][4];
//		atz[i] = Data[i][5];
//	}
//#ifdef PRINT_GYR_ACC
//		printf("Matrix Gyr");
//	for(int i = 0 ; i < N ; i++){
//		printf("%5d:",i);
//		printf("%10f|",gtx[i]);printf(" %10f|",gty[i]);printf(" %10f|",gtz[i]);
//		printf("%10f|",atx[i]);printf(" %10f|",aty[i]);printf(" %10f",atz[i]);
//		printf("\n");
//}
//	//	printf("Matrix Acc");
//	//	for(int i = 0 ; i < N ; i++){
//	//		printf("i:%d",i);
//	//		printf("\n");
//	//		printf("\n");
//	//}
//#endif
//		float gyroScale = 0.0175f;        //covet deg/s to radian/s for mahony
//	//	%% Process data through AHRS algorithm (calcualte orientation)
//#ifdef PRINT_QANT	
//printf("      q0:       q1:       q2:       q3:\n");
//#endif
//for(int i = 0 ;  i < N ; i++){
//		gtx[i] = gtx[i] * gyroScale;
//		gty[i] = gty[i] * gyroScale;
//		gtz[i] = gtz[i] * gyroScale;
//
//		filter.updateIMU(gtx[i], gty[i], gtz[i], atx[i], aty[i], atz[i]);
//		filter.getQuat(q[i]);
//#ifdef PRINT_QANT
//		printf("%5d:%10.5f|%10.5f|%10.5f|%10.5f\n",i, q[i][0], q[i][1], q[i][2],
//				q[i][3]);
//#endif
//		Quaternion<float> mQuat(q[i][0], q[i][1], q[i][2], q[i][3]);
//		mQuat.Quaternion2Matrix(R[i]);
//		convet(RR[i], R[i]);	
//	}
//#ifdef PRINT_RR
//	printf("Matrix RR");
//	for(int i = 0 ; i < N ; i++){
//		printf("i:%d",i);
//		printf("\n");
//		Matrix.Print((mtx_type*) RR[i], 3, 3);
//}
//#endif
//	//	%% Calculate 'tilt-compensated' accelerometer
//	for(int i = 0 ; i < N ; i++){
//		Acc[i][0] = atx[i];
//		Acc[i][1] = aty[i];
//		Acc[i][2] = atz[i];
//		Matrix.Multiply((mtx_type*) RR[i], (mtx_type*) Acc[i], 3, 3, 1,
//				(mtx_type*) tcAcc[i]);
//	}
//#ifdef PRINT_TCACC
//		printf("Matrix tcAcc");
//		for(int i = 0 ; i < N ; i++){
//		printf("i:%d\n",i);
//		Matrix.Print((mtx_type*) tcAcc[i], 3, 1);
//		
//}
//#endif
////
//////%% Calculate linear acceleration in Earth frame (subtracting gravity)
//	for(int i = 0 ; i < N ;i++){
//		Matrix.Subtract((mtx_type*) tcAcc[i], (mtx_type*) Earth, 3, 1,
//				(mtx_type*) linAcc[i]);
//		float tempLinAcc[3];
//		Matrix.Multiply((mtx_type*) linAcc[i], (mtx_type*) g, 3, 1, 1,
//				(mtx_type*) tempLinAcc);
//		Matrix.Copy((mtx_type*) tempLinAcc, 3, 1, (mtx_type*) linAcc[i]);
//		
//	
//}
//#ifdef PRINT_LINACC
//	printf("Matrix linAcc");
//	for(int i = 0 ; i < N ; i++){
//		printf("\ni:%d,",i);
//		for(int l = 0 ; l< 3 ; l++){
//			printf("%.4f",linAcc[i][l]);
//			printf(",");
//		}	}
//#endif
////%% Calculate linear velocity (integrate acceleartion)
//	for(int i =1 ; i < N ; i++){
//		float tempMultiVel[3];
////			Matrix.Copy((mtx_type*) linVel, 3, 1, (mtx_type*) linVelPre);
//			Matrix.Multiply((mtx_type*) linAcc[i], (mtx_type*) samplePeriod, 3, 1,
//					1, (mtx_type*) tempMultiVel);
//			float tempAddVel[3];
//			Matrix.Add((mtx_type*) linVel[i-1], (mtx_type*) tempMultiVel, 3, 1,
//					(mtx_type*) tempAddVel);
//		Matrix.Copy((mtx_type*) tempAddVel, 3, 1, (mtx_type*) linVel[i]);
//	}
//#ifdef PRINT_LINVEL
//		printf("Matrix linVel");
//	for(int i = 0 ; i < N ; i++){
//		printf("\ni:%d,",i);
//		for(int l = 0 ; l< 3 ; l++){
//			printf("%.4f",linVel[i][l]);
//			printf(",");
//		}
//	}
//#endif
////		%% High-pass filter linear velocity to remove drift
//
//	float CUTOFF = 0.01f;
//	int SAMPLE_RATE = 512;
//	float RC = 1.0/(CUTOFF*2*3.14);
//    float dt = 1.0/SAMPLE_RATE;
//    float alpha = RC/(RC + dt);
//    printf("alpha: %.4f\n",alpha);
////	for(int i = 0 ; i < N; ++i){
////	for(int l = 0 ; l < 3 ; l++){
////		linVel[i][l]=LinVelData[i][l];
////		}
////	}
//    float filteredArray[N][3];
//    filteredArray[0][0]= {0.01801};
//    filteredArray[0][1]= {-0.0505};
//    filteredArray[0][2]= {-0.3213};
//    for (int i = 1; i<N; i++){
//    	for(int  l = 0 ; l < 3  ;l++){
//        filteredArray[i][l] = alpha * (filteredArray[i-1][l] + linVel[i][l] - linVel[i-1][l]); 
//        linVelHP[i][l]=filteredArray[i][l];
//    }
//}
//#ifdef PRINT_LINVELHP
//	for(int i = 0 ; i < N; ++i){
//		printf("\n%5d,",i);
//		for(int  l = 0 ; l  < 3 ; l++){
//			printf("%.4f",linVelHP[i][l]);
//			printf(",");	
//	}
//		}
//			printf("Matrix LINVELHP");
//#endif
//
////%% Calculate linear possion (integrate vel)
//for(int i = 1 ; i < N; ++i){
//	float tempMultiPos[3];
////	Matrix.Copy((mtx_type*)linPos[i], 3, 1, (mtx_type*)linPosPre);
//	Matrix.Multiply((mtx_type*)linVelHP[i], (mtx_type*)samplePeriod, 3, 1, 1, (mtx_type*)tempMultiPos);
//	float tempAddPos[3];
//	Matrix.Add((mtx_type*) linPos[i-1], (mtx_type*) tempMultiPos, 3,1, (mtx_type*) tempAddPos);
//	Matrix.Copy((mtx_type*)tempAddPos, 3, 1, (mtx_type*)linPos[i]);	
//}
//#ifdef PRINT_LINPOS
//
//	for(int i = 0 ; i < N; ++i){
//		printf("\n%5d,",i);
//		for(int  l = 0 ; l  < 3 ; l++){
//			printf("%.4f",linPos[i][l]);
//			printf(",");	
//	}
//		}
//	printf("Matrix linPos");
//#endif
//////%% High-pass filter linear pos to remove drift
//    filteredArray[0][0]= {0.0118};
//    filteredArray[0][1]= {0.0142};
//    filteredArray[0][2]= {0.1528};
//    for (int i = 1; i<N; i++){
//    	for(int  l = 0 ; l < 3  ;l++){
//        filteredArray[i][l] = alpha * (filteredArray[i-1][l] + linPos[i][l] - linPos[i-1][l]); 
//    }
//}
//#ifdef PRINT_LINPOSHP
//	printf("Matrix linPosHP");
//	for(int i = 0 ; i < N; ++i){
//		printf("\n%5d,",i);
//		for(int  l = 0 ; l  < 3 ; l++){
//			linPosHP[i][l]=filteredArray[i][l];
//			printf("%.4f",linPosHP[i][l]);
//			printf(",");	
//	}
//
//		}
//#endif

return 0;
}
int convet(float RR[3][3], float R[9]) {
	int i, j;
	for (i = 0; i < 3; ++i)
		for (j = 0; j < 3; ++j) {
			RR[i][j] = R[i * 3 + j];
		}
}


