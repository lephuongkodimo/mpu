#include <MPU6050_tockn.h>
#include <MatrixMath.h>
#include "MadgwickAHRS.h"
#include "MahonyAHRS.h"
#include "quaternion.h"
#include <Wire.h>
MPU6050 mpu6050(Wire);
float testFrequency = 2;
//Madgwick filter;
Mahony filter;
unsigned long microsPerReading, microsPrevious;
float accelScale, gyroScale;
void convet(float RR[3][3], float R[9]);

<<<<<<< HEAD
float samplePeriod[1] = { 0.002 };
float earth[3] = { 0, 0, 1 };
int fc = 0;
=======
  float samplePeriod[1]={0.002};
  float Earth[3]={0,0,1};
  float g[1] = {9.81};
    int fc = 0;
>>>>>>> 0815448194297616c9d40a1a375ae04f174797e1

float linVel[3] = { 0, 0, 0 };
float linVelPre[3];
float linPos[3] = { 0, 0, 0 };
float linPosPre[3];

//HP vel
float mVelEMA_a[1] = { 0.3 };    //initialization of EMA alpha
float mVelOne[1] = { 1 };
float mVelEMA_S[3] = { 0, 0, 0 };        //initialization of EMA S
float linVelHP[3];

//HP pos
float mPosEMA_a[1] = { 0.3 };    //initialization of EMA alpha
float mPosOne[1] = { 1 };
float mPosEMA_S[3] = { 0, 0, 0 };        //initialization of EMA S
float linPosHP[3];
class HigPassFilter {
public:
	HigPassFilter(float reduced_frequency) :
			alpha(1 - exp(-2 * M_PI * reduced_frequency)), y(0) {
	}
	float operator()(float x) {
		y += alpha * (x - y);
		return x - y;
	}
private:
	float alpha, y;
};
HigPassFilter highpassFilter_x(10 / 512);
void setup() {
	Serial.begin(115200);
	Wire.begin(21, 22, 100000);
	mpu6050.begin();
	mpu6050.calcGyroOffsets(true);
	filter.begin(512);

//  // Set the accelerometer range to 2G
//  CurieIMU.setAccelerometerRange(2);
//  // Set the gyroscope range to 250 degrees/second
//  CurieIMU.setGyroRange(250);

	// initialize variables to pace updates to correct rate
	microsPerReading = 1000000 / 512;
	microsPrevious = micros();
}

void loop() {
	fc++;

	float q[4];
	float R[9];
	float RR[3][3];
	float ax, ay, az;
	float gx, gy, gz;
	float Acc[3];
	float tcAcc[3];
	float linAcc[3];
	float roll, pitch, heading;
	unsigned long microsNow;

	// check if it's time to read data and update the filter
	microsNow = micros();
	if (microsNow - microsPrevious >= microsPerReading) {
		mpu6050.update();
		gx = mpu6050.getGyroX();
		gy = mpu6050.getGyroY();
		gz = mpu6050.getGyroZ();        //deg/s
		ax = mpu6050.getAccX();
		ay = mpu6050.getAccY();        //g
		az = mpu6050.getAccZ();
		Serial.println("");
		Serial.printf("ax: %0.3f ay: %0.3f az:%0.3f", ax, ay, az);
		Serial.println("");
		Serial.printf("gx: %0.3f gy: %0.3f gz:%0.3f", gx, gy, gz);
		Serial.println("");

		// update the filter, which computes orientation
		filter.updateIMU(gx, gy, gz, ax, ay, az);

<<<<<<< HEAD
=======
    float gyroScale = 0.0175f;
    gx = gx * gyroScale;
    gy = gy * gyroScale;
    gz = gz * gyroScale;
    // update the filter, which computes orientation
    filter.updateIMU(gx, gy, gz, ax, ay, az);
    
>>>>>>> 0815448194297616c9d40a1a375ae04f174797e1
//Process data through AHRS algorithm (calcualte orientation)
		filter.getQuat(q);
		Serial.printf("q0: %0.3f q1: %0.3f q2:%0.3f q3: %.3f", q[0], q[1], q[2],
				q[3]);
		Serial.println("");
		Quaternion<float> mQuat(q[0], q[1], q[2], q[3]);
		mQuat.Quaternion2Matrix(R);
		for (int i = 0; i < 9; i++) {
			Serial.printf("%0.3f-", R[i]);
		}
		Serial.println("");
		//%% Calculate 'tilt-compensated' accelerometer
		convet(RR, R);

		Acc[1] = ax;
		Acc[2] = ay;
		Acc[3] = az;

		Matrix.Multiply((mtx_type*) RR, (mtx_type*) Acc, 3, 3, 1,
				(mtx_type*) tcAcc);
		Matrix.Print((mtx_type*) tcAcc, 3, 1, "tcAcc");
//
//
////%% Calculate linear velocity (integrate acceleartion)
//  float tempMultiVel[3];
//if(fc > 1){
//  Matrix.Copy((mtx_type*)linVel, 3, 1, (mtx_type*)linVelPre);
//
//  Matrix.Multiply((mtx_type*)linAcc, (mtx_type*)samplePeriod, 3, 1, 1, (mtx_type*)tempMultiVel);
//  Matrix.Add((mtx_type*) linVelPre, (mtx_type*) tempMultiVel, 3,1, (mtx_type*) linVel);
//  Matrix.Print((mtx_type*)linVel, 3, 1,"linVel");
//  
//  
//}
//
////%% High-pass filter linear velocity to remove drift
//
//float tempMultiVelHP[3];
//Matrix.Multiply((mtx_type*)linVel, (mtx_type*)mVelEMA_a, 3, 1, 1, (mtx_type*)tempMultiVelHP);
//float subTempVel[1];
//Matrix.Subtract((mtx_type*) mVelOne, (mtx_type*) mVelEMA_a, 1,1, (mtx_type*) subTempVel);
//float tempMultiHPEMA[3];
//Matrix.Multiply((mtx_type*)mVelEMA_S, (mtx_type*)subTempVel, 3, 1, 1, (mtx_type*)tempMultiHPEMA);
//  Matrix.Add((mtx_type*) tempMultiVelHP, (mtx_type*) tempMultiHPEMA, 3,1, (mtx_type*) mVelEMA_S);
//  Matrix.Subtract((mtx_type*) linVel, (mtx_type*) mVelEMA_S, 3,1, (mtx_type*) linVelHP);
//
////%% Calculate linear possion (integrate vel)
//float tempMultiPos[3];
//if(fc > 1){
//  Matrix.Copy((mtx_type*)linPos, 3, 1, (mtx_type*)linPosPre);
//
//  Matrix.Multiply((mtx_type*)linVelHP, (mtx_type*)samplePeriod, 3, 1, 1, (mtx_type*)tempMultiPos);
//  Matrix.Add((mtx_type*) linPosPre, (mtx_type*) tempMultiPos, 3,1, (mtx_type*) linPos);
//  Matrix.Print((mtx_type*)linPos, 3, 1,"linPos");
//  
//  
//}
////%% High-pass filter linear pos to remove drift
//
//
//float tempMultiPosHP[3];
//Matrix.Multiply((mtx_type*)linPos, (mtx_type*)mPosEMA_a, 3, 1, 1, (mtx_type*)tempMultiPosHP);
//float subTempPos[1];
//Matrix.Subtract((mtx_type*) mPosOne, (mtx_type*) mPosEMA_a, 1,1, (mtx_type*) subTempPos);
//float tempMultiHPEMAPos[3];
//Matrix.Multiply((mtx_type*)mPosEMA_S, (mtx_type*)subTempPos, 3, 1, 1, (mtx_type*)tempMultiHPEMAPos);
//  Matrix.Add((mtx_type*) tempMultiPosHP, (mtx_type*) tempMultiHPEMAPos, 3,1, (mtx_type*) mPosEMA_S);
//  
//  Matrix.Subtract((mtx_type*) linPos, (mtx_type*) mPosEMA_S, 3,1, (mtx_type*) linPosHP);
//
//Matrix.Print((mtx_type*)linPosHP, 3, 1,"linPosHP");
//  
//    // print the heading, pitch and roll
//    roll = filter.getRoll();
//    pitch = filter.getPitch();
//    heading = filter.getYaw();
//    Serial.print("Orientation: ");
//    Serial.print(heading);
//    Serial.print(" ");
//    Serial.print(pitch);
//    Serial.print(" ");
//    Serial.println(roll);
//     Serial.println("+++++++++++++++++++++++++++++++++++++ ");
// Serial.print("Quan:"); Serial.print(q[0]);
// Serial.print(q[1]);Serial.print(q[2]);Serial.print(q[3]);
// Serial.println("+++++++++++++++++++++++++++++++++++++ ");
//  Serial.print("R:"); Serial.print(R[0]);Serial.print(R[1]);Serial.println(R[2]);
//  Serial.print(R[3]);Serial.print(R[4]);Serial.println(R[5]);
//  Serial.print(R[6]);Serial.print(R[7]);Serial.println(R[8]);
//Serial.println("+++++++++++++++++++++++++++++++++++++ ");
//Serial.println("+++++++++++++++++++++++++++++++++++++ ");
//    // increment previous time, so we keep proper pace
		microsPrevious = microsPrevious + microsPerReading;
	}
}

void convet(float RR[3][3], float R[9]) {
	int i, j;
	for (i = 0; i < 3; ++i)
		for (j = 0; j < 3; ++j) {
			RR[i][j] = R[i * 3 + j];
		}
}
