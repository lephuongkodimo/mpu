/*
 This example code is in the Public Domain (or CC0 licensed, at your option.)

 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.
 */

/****************************************************************************
 *
 * This file is for gatt client. It can scan ble device, connect one device.
 * Run the gatt_server demo, the client demo will automatically connect to the gatt_server demo.
 * Client demo will enable gatt_server's notify after connection. Then the two devices will exchange
 * data.
 *
 ****************************************************************************/

#include <stdint.h>
#include <string.h>
#include "string.h"
#include <stdbool.h>
#include <stdio.h>
#include "nvs.h"
#include "nvs_flash.h"
//#include "controller.h"

#include "esp_bt.h"
#include "esp_gap_ble_api.h"
#include "esp_gattc_api.h"
#include "esp_gatt_defs.h"
#include "esp_bt_main.h"
#include "esp_gatt_common_api.h"

#include <sys/socket.h>
#include <netdb.h>
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_event_loop.h"
#include "esp_log.h"
//#include "esp_ota_ops.h"
#include "errno.h"
#include "nvs_flash.h"

//
bool on_ucf ;
#include "esp_gatts_api.h"

////include OTA///////
//
#define GATTS_TAG "GATTS_DEMO"
#define GATTC_TAG "GATTC_DEMO"
#define REMOTE_SERVICE_UUID        0x00FF
#define REMOTE_NOTIFY_CHAR_UUID    0xFF01
#define PROFILE_NUM      1
#define PROFILE_A_APP_ID 0
#define INVALID_HANDLE   0
#define COUT_SHUTDOWN_BLE 8
#define USER_BLE_MODE CF_BLE_CLIENT
#define TOSERVER "toSERVER"
#define TOCLIENT "toCLIENT"
//extern GSConfig mcf;
typedef void (*esp_gatts_status_cb_pt)(const struct event *evt, void *param);
int esp_gatts_status_cb_register(esp_gatts_status_cb_pt cb, void *param) {
	return 0;
}

static const char remote_device_name[] = "GOBOT";
static  bool on_ble_server =false ;
static bool GATT_SERVER = false;
static bool connect_bt = false;
static bool get_server = false;
static uint8_t cout_shutdown_ble = COUT_SHUTDOWN_BLE; //MAX TIMEOUT BLE*cout_shutdown_ble = 30*10=300s

static esp_gattc_char_elem_t *char_elem_result = NULL;
static esp_gattc_descr_elem_t *descr_elem_result = NULL;

/* eclare static functions */
static void esp_gap_cb(esp_gap_ble_cb_event_t event,
		esp_ble_gap_cb_param_t *param);
static void esp_gattc_cb(esp_gattc_cb_event_t event, esp_gatt_if_t gattc_if,
		esp_ble_gattc_cb_param_t *param);
static void gattc_profile_event_handler(esp_gattc_cb_event_t event,
		esp_gatt_if_t gattc_if, esp_ble_gattc_cb_param_t *param);

static esp_bt_uuid_t remote_filter_service_uuid = { .len = ESP_UUID_LEN_16,
		.uuid = { .uuid16 = REMOTE_SERVICE_UUID, }, };

static esp_bt_uuid_t remote_filter_char_uuid = { .len = ESP_UUID_LEN_16, .uuid =
		{ .uuid16 = REMOTE_NOTIFY_CHAR_UUID, }, };

static esp_bt_uuid_t notify_descr_uuid = { .len = ESP_UUID_LEN_16, .uuid = {
		.uuid16 = ESP_GATT_UUID_CHAR_CLIENT_CONFIG, }, };

static esp_ble_scan_params_t ble_scan_params = { .scan_type =
		BLE_SCAN_TYPE_ACTIVE, .own_addr_type = BLE_ADDR_TYPE_PUBLIC,
		.scan_filter_policy = BLE_SCAN_FILTER_ALLOW_ALL, .scan_interval = 0x50,
		.scan_window = 0x30 };

struct gattc_profile_inst_p {
	esp_gattc_cb_t gattc_cb;
	uint16_t gattc_if;
	uint16_t app_id;
	uint16_t conn_id;
	uint16_t service_start_handle;
	uint16_t service_end_handle;
	uint16_t char_handle;
	esp_bd_addr_t remote_bda;
	esp_ble_addr_type_t ble_addr_type;          /*!< Ble device address type */
};
//=========================================//
//-------------------------------------------
//---------------GATTS----------------------
//=========================================//
#define GATTS_SERVICE_UUID_TEST_A   0x00FF
#define GATTS_CHAR_UUID_TEST_A      0xFF01
#define GATTS_DESCR_UUID_TEST_A     0x3333
#define GATTS_NUM_HANDLE_TEST_A     4
#define TEST_DEVICE_NAME            "ESP_GATTS_DEMO"
#define TEST_MANUFACTURER_DATA_LEN  17
#define GATTS_DEMO_CHAR_VAL_LEN_MAX 0x40
#define PREPARE_BUF_MAX_SIZE 1024

uint8_t char1_str[] = {0x11,0x22,0x33};
esp_gatt_char_prop_t a_property = 0;
esp_attr_value_t gatts_demo_char1_val =
{
    .attr_max_len = GATTS_DEMO_CHAR_VAL_LEN_MAX,
    .attr_len     = sizeof(char1_str),
    .attr_value   = char1_str,
};

static uint8_t adv_config_done = 0;
#define adv_config_flag      (1 << 0)
#define scan_rsp_config_flag (1 << 1)
#ifdef CONFIG_SET_RAW_ADV_DATA
static uint8_t raw_adv_data[] = {
        0x02, 0x01, 0x06,
        0x02, 0x0a, 0xeb, 0x03, 0x03, 0xab, 0xcd
};
static uint8_t raw_scan_rsp_data[] = {
        0x0f, 0x09, 0x45, 0x53, 0x50, 0x5f, 0x47, 0x41, 0x54, 0x54, 0x53, 0x5f, 0x44,
        0x45, 0x4d, 0x4f
};
#else
static uint8_t adv_service_uuid128[32] = {
    /* LSB <--------------------------------------------------------------------------------> MSB */
    //first uuid, 16bit, [12],[13] is the value
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xEE, 0x00, 0x00, 0x00,
    //second uuid, 32bit, [12], [13], [14], [15] is the value
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00,
};

// The length of adv data must be less than 31 bytes
//static uint8_t test_manufacturer[TEST_MANUFACTURER_DATA_LEN] =  {0x12, 0x23, 0x45, 0x56};
//adv data
static esp_ble_adv_data_t adv_data = {
    .set_scan_rsp = false,
    .include_name = true,
    .include_txpower = true,
    .min_interval = 0x20,
    .max_interval = 0x40,
    .appearance = 0x00,
    .manufacturer_len = 0, //TEST_MANUFACTURER_DATA_LEN,
    .p_manufacturer_data =  NULL, //&test_manufacturer[0],
    .service_data_len = 0,
    .p_service_data = NULL,
    .service_uuid_len = 32,
    .p_service_uuid = adv_service_uuid128,
    .flag = (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT),
};
// scan response data
static esp_ble_adv_data_t scan_rsp_data = {
    .set_scan_rsp = true,
    .include_name = true,
    .include_txpower = true,
    .min_interval = 0x20,
    .max_interval = 0x40,
    .appearance = 0x00,
    .manufacturer_len = 0, //TEST_MANUFACTURER_DATA_LEN,
    .p_manufacturer_data =  NULL, //&test_manufacturer[0],
    .service_data_len = 0,
    .p_service_data = NULL,
    .service_uuid_len = 32,
    .p_service_uuid = adv_service_uuid128,
    .flag = (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT),
};

#endif /* CONFIG_SET_RAW_ADV_DATA */
esp_bd_addr_t peer_addr_p = {0x30,0xae,0xa4,0x28,0xb4,0x56};
static esp_ble_adv_params_t adv_params = {
    adv_int_min        : 0x20,
    adv_int_max       : 0x40,
    adv_type           : ADV_TYPE_IND,
    own_addr_type      :BLE_ADDR_TYPE_PUBLIC,
    peer_addr            :{0x30,0xae,0xa4,0x28,0xb4,0x56} ,
    peer_addr_type       : BLE_ADDR_TYPE_PUBLIC,
    channel_map       : ADV_CHNL_ALL,
    adv_filter_policy : ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY,
};
#define PROFILE_NUM_S 1

#define PROFILE_A_APP_ID_S 0
static void gatts_profile_a_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param);

struct gatts_profile_inst {
    esp_gatts_cb_t gatts_cb;
    uint16_t gatts_if;
    uint16_t app_id;
    uint16_t conn_id;
    uint16_t service_handle;
    esp_gatt_srvc_id_t service_id;
    uint16_t char_handle;
    esp_bt_uuid_t char_uuid;
    esp_gatt_perm_t perm;
    esp_gatt_char_prop_t property;
    uint16_t descr_handle;
    esp_bt_uuid_t descr_uuid;
};

/* One gatt-based profile one app_id and one gatts_if, this array will store the gatts_if returned by ESP_GATTS_REG_EVT */
static struct gatts_profile_inst gl_profile_tab_s[PROFILE_NUM_S] = {
    [PROFILE_A_APP_ID_S] = {
        .gatts_cb = gatts_profile_a_event_handler,
        .gatts_if = ESP_GATT_IF_NONE,       /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
    },

};


typedef struct {
    uint8_t                 *prepare_buf;
    int                     prepare_len;
} prepare_type_env_t;

static prepare_type_env_t a_prepare_write_env;
//when reciving data
//....[data 20 byte]..............[data 20byte][flag exec long write]
//num data = MTU
void example_write_event_env(esp_gatt_if_t gatts_if, prepare_type_env_t *prepare_write_env, esp_ble_gatts_cb_param_t *param);
//when recived exec_long_write_flasg(same prepare write flag )
void example_exec_write_event_env(prepare_type_env_t *prepare_write_env, esp_ble_gatts_cb_param_t *param);


void example_write_event_env(esp_gatt_if_t gatts_if, prepare_type_env_t *prepare_write_env, esp_ble_gatts_cb_param_t *param){
    esp_gatt_status_t status = ESP_GATT_OK;
    if (param->write.need_rsp){
        if (param->write.is_prep){
            if (prepare_write_env->prepare_buf == NULL) {
                prepare_write_env->prepare_buf = (uint8_t *)malloc(PREPARE_BUF_MAX_SIZE*sizeof(uint8_t));
                prepare_write_env->prepare_len = 0;
                if (prepare_write_env->prepare_buf == NULL) {
                    ESP_LOGE(GATTS_TAG, "Gatt_server prep no mem\n");
                    status = ESP_GATT_NO_RESOURCES;
                }
            } else {
                if(param->write.offset > PREPARE_BUF_MAX_SIZE) {
                    status = ESP_GATT_INVALID_OFFSET;
                } else if ((param->write.offset + param->write.len) > PREPARE_BUF_MAX_SIZE) {
                    status = ESP_GATT_INVALID_ATTR_LEN;
                }
            }

            esp_gatt_rsp_t *gatt_rsp = (esp_gatt_rsp_t *)malloc(sizeof(esp_gatt_rsp_t));
            gatt_rsp->attr_value.len = param->write.len;
            gatt_rsp->attr_value.handle = param->write.handle;
            gatt_rsp->attr_value.offset = param->write.offset;
            gatt_rsp->attr_value.auth_req = ESP_GATT_AUTH_REQ_NONE;
            memcpy(gatt_rsp->attr_value.value, param->write.value, param->write.len);
            esp_err_t response_err = esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, status, gatt_rsp);
            if (response_err != ESP_OK){
               ESP_LOGE(GATTS_TAG, "Send response error\n");
            }
            free(gatt_rsp);
            if (status != ESP_GATT_OK){
                return;
            }
            memcpy(prepare_write_env->prepare_buf + param->write.offset,
                   param->write.value,
                   param->write.len);
            prepare_write_env->prepare_len += param->write.len;

        }else{
            esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, status, NULL);
        }
    }
}
void example_exec_write_event_env(prepare_type_env_t *prepare_write_env, esp_ble_gatts_cb_param_t *param){
    if (param->exec_write.exec_write_flag == ESP_GATT_PREP_WRITE_EXEC){
        esp_log_buffer_hex(GATTS_TAG, prepare_write_env->prepare_buf, prepare_write_env->prepare_len);
    }else{
        ESP_LOGI(GATTS_TAG,"ESP_GATT_PREP_WRITE_CANCEL");
    }
    if (prepare_write_env->prepare_buf) {
        free(prepare_write_env->prepare_buf);
        prepare_write_env->prepare_buf = NULL;
    }
    prepare_write_env->prepare_len = 0;
}



static void gatts_profile_a_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param) {

	if (event == ESP_GATTS_REG_EVT) {
        ESP_LOGI(GATTS_TAG, "REGISTER_APP_EVT, status %d, app_id %d\n", param->reg.status, param->reg.app_id);
        gl_profile_tab_s[PROFILE_A_APP_ID_S].service_id.is_primary = true;
        gl_profile_tab_s[PROFILE_A_APP_ID_S].service_id.id.inst_id = 0x00;
        gl_profile_tab_s[PROFILE_A_APP_ID_S].service_id.id.uuid.len = ESP_UUID_LEN_16;
        gl_profile_tab_s[PROFILE_A_APP_ID_S].service_id.id.uuid.uuid.uuid16 = GATTS_SERVICE_UUID_TEST_A;

        esp_err_t set_dev_name_ret = esp_ble_gap_set_device_name(TEST_DEVICE_NAME);
        if (set_dev_name_ret){
            ESP_LOGE(GATTS_TAG, "set device name failed, error code = %x", set_dev_name_ret);
        }
#ifdef CONFIG_SET_RAW_ADV_DATA
        esp_err_t raw_adv_ret = esp_ble_gap_config_adv_data_raw(raw_adv_data, sizeof(raw_adv_data));
        if (raw_adv_ret){
            ESP_LOGE(GATTS_TAG, "config raw adv data failed, error code = %x ", raw_adv_ret);
        }
        adv_config_done |= adv_config_flag;
        esp_err_t raw_scan_ret = esp_ble_gap_config_scan_rsp_data_raw(raw_scan_rsp_data, sizeof(raw_scan_rsp_data));
        if (raw_scan_ret){
            ESP_LOGE(GATTS_TAG, "config raw scan rsp data failed, error code = %x", raw_scan_ret);
        }
        adv_config_done |= scan_rsp_config_flag;
#else
        //config adv data
        esp_err_t ret = esp_ble_gap_config_adv_data(&adv_data);
        if (ret){
            ESP_LOGE(GATTS_TAG, "config adv data failed, error code = %x", ret);
        }
        adv_config_done |= adv_config_flag;
        //config scan response data
        ret = esp_ble_gap_config_adv_data(&scan_rsp_data);
        if (ret){
            ESP_LOGE(GATTS_TAG, "config scan response data failed, error code = %x", ret);
        }
        adv_config_done |= scan_rsp_config_flag;

#endif
        esp_ble_gatts_create_service(gatts_if, &gl_profile_tab_s[PROFILE_A_APP_ID_S].service_id, GATTS_NUM_HANDLE_TEST_A);
        return;
	}
	 else if (event ==ESP_GATTS_READ_EVT){
        ESP_LOGI(GATTS_TAG, "GATT_READ_EVT, conn_id %d, trans_id %d, handle %d\n", param->read.conn_id, param->read.trans_id, param->read.handle);
        esp_gatt_rsp_t rsp;
        memset(&rsp, 0, sizeof(esp_gatt_rsp_t));
        rsp.attr_value.handle = param->read.handle;
        rsp.attr_value.len = 4;
        rsp.attr_value.value[0] = 0xde;
        rsp.attr_value.value[1] = 0xed;
        rsp.attr_value.value[2] = 0xbe;
        rsp.attr_value.value[3] = 0xef;
        esp_ble_gatts_send_response(gatts_if, param->read.conn_id, param->read.trans_id,
                                    ESP_GATT_OK, &rsp);
        return;
    }
	 else if (event == ESP_GATTS_WRITE_EVT) {
        ESP_LOGI(GATTS_TAG, "GATT_WRITE_EVT, conn_id %d, trans_id %d, handle %d", param->write.conn_id, param->write.trans_id, param->write.handle);
        // neu khong phia la mot ghi dai // long write // < mut size
        if (!param->write.is_prep){
            ESP_LOGI(GATTS_TAG, "GATT_WRITE_EVT, value len %d, value :", param->write.len);
            esp_log_buffer_hex(GATTS_TAG, param->write.value, param->write.len);
            if (gl_profile_tab_s[PROFILE_A_APP_ID_S].descr_handle == param->write.handle && param->write.len == 2){
                uint16_t descr_value = param->write.value[1]<<8 | param->write.value[0];
                if (descr_value == 0x0001){
                    if (a_property & ESP_GATT_CHAR_PROP_BIT_NOTIFY){
                        ESP_LOGI(GATTS_TAG, "notify enable");
                        uint8_t notify_data[15];
                        for (int i = 0; i < sizeof(notify_data); ++i)
                        {
                            notify_data[i] = i%0xff;
                        }
                        //the size of notify_data[] need less than MTU size
                        esp_ble_gatts_send_indicate(gatts_if, param->write.conn_id, gl_profile_tab_s[PROFILE_A_APP_ID_S].char_handle,
                                                sizeof(notify_data), notify_data, false);
                    }
                }else if (descr_value == 0x0002){
                    if (a_property & ESP_GATT_CHAR_PROP_BIT_INDICATE){
                        ESP_LOGI(GATTS_TAG, "indicate enable");
                        uint8_t indicate_data[15];
                        for (int i = 0; i < sizeof(indicate_data); ++i)
                        {
                            indicate_data[i] = i%0xff;
                        }
                        //the size of indicate_data[] need less than MTU size
                        esp_ble_gatts_send_indicate(gatts_if, param->write.conn_id, gl_profile_tab_s[PROFILE_A_APP_ID_S].char_handle,
                                                sizeof(indicate_data), indicate_data, true);
                    }
                }
                else if (descr_value == 0x0000){
                    ESP_LOGI(GATTS_TAG, "notify/indicate disable ");
                }else{
                    ESP_LOGE(GATTS_TAG, "unknown descr value");
                    esp_log_buffer_hex(GATTS_TAG, param->write.value, param->write.len);
                }

            }
        }
        char toCLIENT[9] = TOCLIENT;
        if(memcmp(toCLIENT,param->write.value,sizeof(toCLIENT))==0)
         {

        	on_ucf = false;
        	GATT_SERVER = false;
        	on_ble_server = false;
        	vTaskDelay(1000/portTICK_PERIOD_MS);

        }
        //neu al gi dai // vo dau chuan bi buffer blabla// ghep noi
        example_write_event_env(gatts_if, &a_prepare_write_env, param);
        return;
    }
	//ghi dai xong nhan duoc co ket thuc ghi dai
	 else if (event == ESP_GATTS_EXEC_WRITE_EVT){
        ESP_LOGI(GATTS_TAG,"ESP_GATTS_EXEC_WRITE_EVT");
        esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, ESP_GATT_OK, NULL);
        example_exec_write_event_env(&a_prepare_write_env, param);
        return;
	 }
	 else if (event == ESP_GATTS_MTU_EVT){
        ESP_LOGI(GATTS_TAG, "ESP_GATTS_MTU_EVT, MTU %d", param->mtu.mtu);
        return;
	 }
	 else if (event ==ESP_GATTS_UNREG_EVT){
		 return;
	 }
	 else if (event ==ESP_GATTS_CREATE_EVT){
        ESP_LOGI(GATTS_TAG, "CREATE_SERVICE_EVT, status %d,  service_handle %d\n", param->create.status, param->create.service_handle);
        gl_profile_tab_s[PROFILE_A_APP_ID_S].service_handle = param->create.service_handle;
        gl_profile_tab_s[PROFILE_A_APP_ID_S].char_uuid.len = ESP_UUID_LEN_16;
        gl_profile_tab_s[PROFILE_A_APP_ID_S].char_uuid.uuid.uuid16 = GATTS_CHAR_UUID_TEST_A;

        esp_ble_gatts_start_service(gl_profile_tab_s[PROFILE_A_APP_ID_S].service_handle);
        a_property = ESP_GATT_CHAR_PROP_BIT_READ | ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_NOTIFY;
        esp_err_t add_char_ret = esp_ble_gatts_add_char(gl_profile_tab_s[PROFILE_A_APP_ID_S].service_handle, &gl_profile_tab_s[PROFILE_A_APP_ID_S].char_uuid,
                                                        ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
                                                        a_property,
                                                        &gatts_demo_char1_val, NULL);
        if (add_char_ret){
            ESP_LOGE(GATTS_TAG, "add char failed, error code =%x",add_char_ret);
        }
        return;
	 }
	 else if (event ==ESP_GATTS_ADD_INCL_SRVC_EVT){
		 return;
	 }
	 else if (event ==ESP_GATTS_ADD_CHAR_EVT){
        uint16_t length = 0;
        const uint8_t *prf_char;

        ESP_LOGI(GATTS_TAG, "ADD_CHAR_EVT, status %d,  attr_handle %d, service_handle %d\n",
                param->add_char.status, param->add_char.attr_handle, param->add_char.service_handle);
        gl_profile_tab_s[PROFILE_A_APP_ID_S].char_handle = param->add_char.attr_handle;
        gl_profile_tab_s[PROFILE_A_APP_ID_S].descr_uuid.len = ESP_UUID_LEN_16;
        gl_profile_tab_s[PROFILE_A_APP_ID_S].descr_uuid.uuid.uuid16 = ESP_GATT_UUID_CHAR_CLIENT_CONFIG;
        esp_err_t get_attr_ret = esp_ble_gatts_get_attr_value(param->add_char.attr_handle,  &length, &prf_char);
        if (get_attr_ret == ESP_FAIL){
            ESP_LOGE(GATTS_TAG, "ILLEGAL HANDLE");
        }

        ESP_LOGI(GATTS_TAG, "the gatts demo char length = %x\n", length);
        for(int i = 0; i < length; i++){
            ESP_LOGI(GATTS_TAG, "prf_char[%x] =%x\n",i,prf_char[i]);
        }
        esp_err_t add_descr_ret = esp_ble_gatts_add_char_descr(gl_profile_tab_s[PROFILE_A_APP_ID_S].service_handle, &gl_profile_tab_s[PROFILE_A_APP_ID_S].descr_uuid,
                                                                ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, NULL, NULL);
        if (add_descr_ret){
            ESP_LOGE(GATTS_TAG, "add char descr failed, error code =%x", add_descr_ret);
        }
        return;
    }
	 else if (event ==ESP_GATTS_ADD_CHAR_DESCR_EVT){
        gl_profile_tab_s[PROFILE_A_APP_ID_S].descr_handle = param->add_char_descr.attr_handle;
        ESP_LOGI(GATTS_TAG, "ADD_DESCR_EVT, status %d, attr_handle %d, service_handle %d\n",
                 param->add_char_descr.status, param->add_char_descr.attr_handle, param->add_char_descr.service_handle);
        return;
	 }
	 else if (event == ESP_GATTS_DELETE_EVT){
		 return;
    }
    else if (event ==ESP_GATTS_START_EVT){
        ESP_LOGI(GATTS_TAG, "SERVICE_START_EVT, status %d, service_handle %d\n",
                 param->start.status, param->start.service_handle);
        return;
    }
    else if (event == ESP_GATTS_STOP_EVT){
    	return;
    }
    else if (event ==ESP_GATTS_CONNECT_EVT) {
        esp_ble_conn_update_params_t conn_params = {0};
        memcpy(conn_params.bda, param->connect.remote_bda, sizeof(esp_bd_addr_t));
        /* For the IOS system, please reference the apple official documents about the ble connection parameters restrictions. */
        conn_params.latency = 0;
        conn_params.max_int = 0x20;    // max_int = 0x20*1.25ms = 40ms
        conn_params.min_int = 0x10;    // min_int = 0x10*1.25ms = 20ms
        conn_params.timeout = 400;    // timeout = 400*10ms = 4000ms
        ESP_LOGI(GATTS_TAG, "ESP_GATTS_CONNECT_EVT, conn_id %d, remote %02x:%02x:%02x:%02x:%02x:%02x:",
                 param->connect.conn_id,
                 param->connect.remote_bda[0], param->connect.remote_bda[1], param->connect.remote_bda[2],
                 param->connect.remote_bda[3], param->connect.remote_bda[4], param->connect.remote_bda[5]);
        gl_profile_tab_s[PROFILE_A_APP_ID_S].conn_id = param->connect.conn_id;
        //start sent the update connection parameters to the peer device.
        esp_ble_gap_update_conn_params(&conn_params);
        return;
    }
    else if (event == ESP_GATTS_DISCONNECT_EVT){
        ESP_LOGI(GATTS_TAG, "ESP_GATTS_DISCONNECT_EVT");
        esp_ble_gap_start_advertising(&adv_params);
        return;
    }
        else if (event == ESP_GATTS_CONF_EVT){
        ESP_LOGI(GATTS_TAG, "ESP_GATTS_CONF_EVT, status %d", param->conf.status);
        if (param->conf.status != ESP_GATT_OK){
            esp_log_buffer_hex(GATTS_TAG, param->conf.value, param->conf.len);
        }
        return;
}
        else if (event == ESP_GATTS_OPEN_EVT){}
        		 else if (event == ESP_GATTS_CANCEL_OPEN_EVT){}
        				 else if (event == ESP_GATTS_CLOSE_EVT){}
        						 else if (event == ESP_GATTS_LISTEN_EVT){}
        								 else if (event == ESP_GATTS_CONGEST_EVT){}
        										 else
        											 return;
    }

//static void gap_event_handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
//{
//    switch (event) {
//#ifdef CONFIG_SET_RAW_ADV_DATA
//    case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT:
//        adv_config_done &= (~adv_config_flag);
//        if (adv_config_done==0){
//            esp_ble_gap_start_advertising(&adv_params);
//        }
//        break;
//    case ESP_GAP_BLE_SCAN_RSP_DATA_RAW_SET_COMPLETE_EVT:
//        adv_config_done &= (~scan_rsp_config_flag);
//        if (adv_config_done==0){
//            esp_ble_gap_start_advertising(&adv_params);
//        }
//        break;
//#else
//    case ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT:
//        adv_config_done &= (~adv_config_flag);
//        if (adv_config_done == 0){
//            esp_ble_gap_start_advertising(&adv_params);
//        }
//        break;
//    case ESP_GAP_BLE_SCAN_RSP_DATA_SET_COMPLETE_EVT:
//        adv_config_done &= (~scan_rsp_config_flag);
//        if (adv_config_done == 0){
//            esp_ble_gap_start_advertising(&adv_params);
//        }
//        break;
//#endif
//    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
//        //advertising start complete event to indicate advertising start successfully or failed
//        if (param->adv_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
//            ESP_LOGE(GATTS_TAG, "Advertising start failed\n");
//        }
//        break;
//    case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
//        if (param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS) {
//            ESP_LOGE(GATTS_TAG, "Advertising stop failed\n");
//        }
//        else {
//            ESP_LOGI(GATTS_TAG, "Stop adv successfully\n");
//        }
//        break;
//    case ESP_GAP_BLE_UPDATE_CONN_PARAMS_EVT:
//         ESP_LOGI(GATTS_TAG, "update connection params status = %d, min_int = %d, max_int = %d,conn_int = %d,latency = %d, timeout = %d",
//                  param->update_conn_params.status,
//                  param->update_conn_params.min_int,
//                  param->update_conn_params.max_int,
//                  param->update_conn_params.conn_int,
//                  param->update_conn_params.latency,
//                  param->update_conn_params.timeout);
//        break;
//    default:
//        break;
//    }
//}

static void gatts_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param)
{
    /* If event is register event, store the gatts_if for each profile */
    if (event == ESP_GATTS_REG_EVT) {
        if (param->reg.status == ESP_GATT_OK) {
            gl_profile_tab_s[param->reg.app_id].gatts_if = gatts_if;
        } else {
            ESP_LOGI(GATTS_TAG, "Reg app failed, app_id %04x, status %d\n",
                    param->reg.app_id,
                    param->reg.status);
            return;
        }
    }

    /* If the gatts_if equal to profile A, call profile A cb handler,
     * so here call each profile's callback */
    do {
        int idx;
        for (idx = 0; idx < PROFILE_NUM_S; idx++) {
            if (gatts_if == ESP_GATT_IF_NONE || /* ESP_GATT_IF_NONE, not specify a certain gatt_if, need to call every profile cb function */
                    gatts_if == gl_profile_tab_s[idx].gatts_if) {
                if (gl_profile_tab_s[idx].gatts_cb) {
                    gl_profile_tab_s[idx].gatts_cb(event, gatts_if, param);
                }
            }
        }
    } while (0);
}

//==end gats==//
/* One gatt-based profile one app_id and one gattc_if, this array will store the gattc_if returned by ESP_GATTS_REG_EVT */
static struct gattc_profile_inst_p gl_profile_tab[PROFILE_NUM] = {
		[PROFILE_A_APP_ID] = {
				.gattc_cb = gattc_profile_event_handler,
				.gattc_if = ESP_GATT_IF_NONE, /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
		},
};

static void gattc_profile_event_handler(esp_gattc_cb_event_t event,
		esp_gatt_if_t gattc_if, esp_ble_gattc_cb_param_t *param) {
	esp_ble_gattc_cb_param_t *p_data = (esp_ble_gattc_cb_param_t *) param;
	if (event == ESP_GATTC_REG_EVT) {
		ESP_LOGI(GATTC_TAG, "REG_EVT");
		esp_err_t scan_ret = esp_ble_gap_set_scan_params(&ble_scan_params);
		if (scan_ret) {
			ESP_LOGE(GATTC_TAG, "set scan params error, error code = %x",
					scan_ret);
		}
		return;

	} else if (event == ESP_GATTC_CONNECT_EVT) {
		ESP_LOGI(GATTC_TAG, "ESP_GATTC_CONNECT_EVT conn_id %d, if %d",
				p_data->connect.conn_id, gattc_if);
		gl_profile_tab[PROFILE_A_APP_ID].conn_id = p_data->connect.conn_id;
		memcpy(gl_profile_tab[PROFILE_A_APP_ID].remote_bda,
				p_data->connect.remote_bda, sizeof(esp_bd_addr_t));
		ESP_LOGI(GATTC_TAG, "REMOTE BDA:");
		esp_log_buffer_hex(GATTC_TAG, gl_profile_tab[PROFILE_A_APP_ID].remote_bda, sizeof(esp_bd_addr_t));
		esp_err_t mtu_ret = esp_ble_gattc_send_mtu_req(gattc_if,
				p_data->connect.conn_id);
		if (mtu_ret) {
			ESP_LOGE(GATTC_TAG, "config MTU error, error code = %x", mtu_ret);
		}
		return;

	} else if (event == ESP_GATTC_OPEN_EVT) {
		if (param->open.status != ESP_GATT_OK) {
			ESP_LOGE(GATTC_TAG, "open failed, status %d", p_data->open.status);
			//	            break;
			return;
		}
		ESP_LOGI(GATTC_TAG, "open success");
		return;

	} else if (event == ESP_GATTC_CFG_MTU_EVT) {
		if (param->cfg_mtu.status != ESP_GATT_OK) {
			ESP_LOGE(GATTC_TAG, "config mtu failed, error status = %x",
					param->cfg_mtu.status);
		}
		ESP_LOGI(GATTC_TAG,
				"ESP_GATTC_CFG_MTU_EVT, Status %d, MTU %d, conn_id %d",
				param->cfg_mtu.status, param->cfg_mtu.mtu,
				param->cfg_mtu.conn_id);
		esp_ble_gattc_search_service(gattc_if, param->cfg_mtu.conn_id,
				&remote_filter_service_uuid);
		return;

	} else if (event == ESP_GATTC_SEARCH_RES_EVT) {
		ESP_LOGI(GATTC_TAG, "ESP_GATTC_SEARCH_RES_EVT");
		esp_gatt_srvc_id_t *srvc_id =
				(esp_gatt_srvc_id_t *) &p_data->search_res.srvc_id;
		if (srvc_id->id.uuid.len == ESP_UUID_LEN_16
				&& srvc_id->id.uuid.uuid.uuid16 == REMOTE_SERVICE_UUID) {
			ESP_LOGI(GATTC_TAG, "service found");
			get_server = true;
			gl_profile_tab[PROFILE_A_APP_ID].service_start_handle =
					p_data->search_res.start_handle;
			gl_profile_tab[PROFILE_A_APP_ID].service_end_handle =
					p_data->search_res.end_handle;
			ESP_LOGI(GATTC_TAG, "UUID16: %x", srvc_id->id.uuid.uuid.uuid16);
		}
		return;

	} else if (event == ESP_GATTC_SEARCH_CMPL_EVT) {

		if (p_data->search_cmpl.status != ESP_GATT_OK) {
			ESP_LOGE(GATTC_TAG, "search service failed, error status = %x",
					p_data->search_cmpl.status);
			//	            break;
			return;
		}
		ESP_LOGI(GATTC_TAG, "ESP_GATTC_SEARCH_CMPL_EVT");
		if (get_server) {
			uint16_t count = 0;
			esp_gatt_status_t status = esp_ble_gattc_get_attr_count(gattc_if,
					p_data->search_cmpl.conn_id, ESP_GATT_DB_CHARACTERISTIC,
					gl_profile_tab[PROFILE_A_APP_ID].service_start_handle,
					gl_profile_tab[PROFILE_A_APP_ID].service_end_handle,
					INVALID_HANDLE, &count);
			if (status != ESP_GATT_OK) {
				ESP_LOGE(GATTC_TAG, "esp_ble_gattc_get_attr_count error");
			}

			if (count > 0) {
				char_elem_result = (esp_gattc_char_elem_t *) malloc(
						sizeof(esp_gattc_char_elem_t) * count);
				if (!char_elem_result) {
					ESP_LOGE(GATTC_TAG, "gattc no mem");
				} else {
					status =
							esp_ble_gattc_get_char_by_uuid(gattc_if,
									p_data->search_cmpl.conn_id,
									gl_profile_tab[PROFILE_A_APP_ID].service_start_handle,
									gl_profile_tab[PROFILE_A_APP_ID].service_end_handle,
									remote_filter_char_uuid, char_elem_result,
									&count);
					if (status != ESP_GATT_OK) {
						ESP_LOGE(GATTC_TAG,
								"esp_ble_gattc_get_char_by_uuid error");
					}

					/*  Every service have only one char in our 'ESP_GATTS_DEMO' demo, so we used first 'char_elem_result' */
					if (count > 0
							&& (char_elem_result[0].properties
									& ESP_GATT_CHAR_PROP_BIT_NOTIFY)) {
						gl_profile_tab[PROFILE_A_APP_ID].char_handle =
								char_elem_result[0].char_handle;
						esp_ble_gattc_register_for_notify(gattc_if,
								gl_profile_tab[PROFILE_A_APP_ID].remote_bda,
								char_elem_result[0].char_handle);
					}
				}
				/* free char_elem_result */
				free(char_elem_result);
			} else {
				ESP_LOGE(GATTC_TAG, "no char found");
			}
		}
		return;
	} else if (event == ESP_GATTC_REG_FOR_NOTIFY_EVT) { //when esp_ble_gattc_register_for_notify() complete
		ESP_LOGI(GATTC_TAG, "ESP_GATTC_REG_FOR_NOTIFY_EVT");
		if (p_data->reg_for_notify.status != ESP_GATT_OK) {
			ESP_LOGE(GATTC_TAG, "REG FOR NOTIFY failed: error status = %d",
					p_data->reg_for_notify.status);
		} else {
			uint16_t count = 0;
			uint16_t notify_en = 1;
			esp_gatt_status_t ret_status = esp_ble_gattc_get_attr_count(
					gattc_if, gl_profile_tab[PROFILE_A_APP_ID].conn_id,
					ESP_GATT_DB_DESCRIPTOR,
					gl_profile_tab[PROFILE_A_APP_ID].service_start_handle,
					gl_profile_tab[PROFILE_A_APP_ID].service_end_handle,
					gl_profile_tab[PROFILE_A_APP_ID].char_handle, &count);
			if (ret_status != ESP_GATT_OK) {
				ESP_LOGE(GATTC_TAG, "esp_ble_gattc_get_attr_count error");
			}
			if (count > 0) {
				descr_elem_result = (esp_gattc_descr_elem_t*) malloc(
						sizeof(esp_gattc_descr_elem_t) * count);
				if (!descr_elem_result) {
					ESP_LOGE(GATTC_TAG, "malloc error, gattc no mem");
				} else {
					ret_status = esp_ble_gattc_get_descr_by_char_handle(
							gattc_if, gl_profile_tab[PROFILE_A_APP_ID].conn_id,
							p_data->reg_for_notify.handle, notify_descr_uuid,
							descr_elem_result, &count);
					if (ret_status != ESP_GATT_OK) {
						ESP_LOGE(GATTC_TAG,
								"esp_ble_gattc_get_descr_by_char_handle error");
					}
					/* Every char has only one descriptor in our 'ESP_GATTS_DEMO' demo, so we used first 'descr_elem_result' */
					//
					if (count
							> 0&& descr_elem_result[0].uuid.len == ESP_UUID_LEN_16 && descr_elem_result[0].uuid.uuid.uuid16 == ESP_GATT_UUID_CHAR_CLIENT_CONFIG) {
						ret_status =
								(esp_gatt_status_t) esp_ble_gattc_write_char_descr(
										gattc_if,
										gl_profile_tab[PROFILE_A_APP_ID].conn_id,
										descr_elem_result[0].handle,
										sizeof(notify_en),
										(uint8_t *) &notify_en,
										ESP_GATT_WRITE_TYPE_RSP,
										ESP_GATT_AUTH_REQ_NONE);
					}

					if (ret_status != ESP_GATT_OK) {
						ESP_LOGE(GATTC_TAG,
								"esp_ble_gattc_write_char_descr error");
					}

					/* free descr_elem_result */
					free(descr_elem_result);
				}
			} else {
				ESP_LOGE(GATTC_TAG, "decsr not found");
			}

		}
		return;

	} else if (event == ESP_GATTC_NOTIFY_EVT) {
		if (p_data->notify.is_notify) {
			ESP_LOGI(GATTC_TAG, "ESP_GATTC_NOTIFY_EVT, receive notify value:");

		} else {
			ESP_LOGI(GATTC_TAG, "ESP_GATTC_NOTIFY_EVT, receive indicate value:");
		}
		esp_log_buffer_hex(GATTC_TAG, p_data->notify.value, p_data->notify.value_len);
		ESP_LOGI(GATTC_TAG, "ESP_GATTC_NOTIFY_EVT, receive notify length: %d",param->notify.value_len);
		ESP_LOGI(GATTC_TAG, "ESP_GATTC_NOTIFY_EVT, receive notify length: %c%c%c",param->notify.value[0],param->notify.value[1],param->notify.value[2]);
		//change mode client <-> server
		if(param->notify.value_len == 0){
			ESP_LOGE(GATTS_TAG, "NO notify" );
			return ;
		}
//		const char * toServer = "toSERVER";
		char write_char_data[9]= TOSERVER ;
//		strcpy(write_char_data,toServer);
//							for (int i = 0; i < sizeof(write_char_data); ++i)
//							{
//								write_char_data[i] = 0xBB;
//							}
							if(memcmp(write_char_data,param->notify.value,sizeof(write_char_data))==0){
				                ESP_LOGI("GATT_SERVER", "............BEIGN change mode ........ ");
				                cout_shutdown_ble  = 1;
				                esp_ble_gap_disconnect(gl_profile_tab[0].remote_bda);

				                on_ble_server = true;
				                GATT_SERVER = true;
				                on_ucf = false;
				                vTaskDelay(1000/portTICK_PERIOD_MS);
				                esp_err_t ret = esp_ble_gattc_app_unregister(PROFILE_A_APP_ID);
				                if (ret){
									ESP_LOGE(GATTS_TAG, "UNregister error, error code = %x", ret);
									return ;
								}


				                ret = esp_ble_gatts_app_register(PROFILE_A_APP_ID_S);
				        	    if (ret){
				        	        ESP_LOGE(GATTS_TAG, "gatts app register error, error code = %x", ret);
				        	        return ;
				        	    }

//				                const esp_partition_t *update_partition = NULL;
//				                const esp_partition_t *running = esp_ota_get_running_partition();
//				                update_partition->address=0x160000;
//				                update_partition->encrypted=false;
//								update_partition->label="ota_0";
//								update_partition->size=0x100000;
//								update_partition->subtype=ESP_PARTITION_SUBTYPE_APP_OTA_0;
//								update_partition->type=ESP_PARTITION_TYPE_APP;
//								const esp_partition_t esp_ble_server_pointer = {
//				                		.type = ESP_PARTITION_TYPE_APP,         /*!< partition type (app/data) */
//										.subtype = ESP_PARTITION_SUBTYPE_APP_OTA_0,    /*!< partition subtype */
//										.address = 0x160000 ,                   /*!< starting address of the partition in flash */
//										.size = 0x100000,                      /*!< size of the partition, in bytes */
//										.label = "ota_0",                     /*!< partition label, zero-terminated ASCII string */
//										.encrypted = false,
//				                };
//								update_partition=&esp_ble_server_pointer;
//				                ESP_LOGI(TAG_OTA, "Running partition labeb %s type %d subtype %d (offset 0x%08x)",
//				                		running->label,running->type, running->subtype, running->address);
//				                ESP_LOGI(TAG_OTA, "configured partition type %d subtype %d (offset 0x%08x)",
//				                		configured->type, configured->subtype, configured->address);
//				                esp_err_t err = esp_ota_set_boot_partition(&esp_ble_server_pointer);
//								if (err != ESP_OK) {
//									ESP_LOGE(TAG_OTA, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(err));
//
//								}
//								ESP_LOGI(TAG_OTA, "Prepare to restart system!");
//								esp_restart();
//								return ;
							}
		//brak
		return;
	} else if (event == ESP_GATTC_WRITE_DESCR_EVT) {
		if (p_data->write.status != ESP_GATT_OK) {
			ESP_LOGE(GATTC_TAG, "write descr failed, error status = %x",
					p_data->write.status);
			//brak
			return;
		}

		ESP_LOGI(GATTC_TAG, "write descr success ");
//            uint8_t write_char_data[35];
//            for (int i = 0; i < sizeof(write_char_data); ++i)
//            {
//                write_char_data[i] = i % 256;
//            }
//            esp_ble_gattc_write_char( gattc_if,
//                                      gl_profile_tab[PROFILE_A_APP_ID].conn_id,
//                                      gl_profile_tab[PROFILE_A_APP_ID].char_handle,
//                                      sizeof(write_char_data),
//                                      write_char_data,
//                                      ESP_GATT_WRITE_TYPE_RSP,
//                                      ESP_GATT_AUTH_REQ_NONE);
		return;
	} else if (event == ESP_GATTC_SRVC_CHG_EVT) {

		esp_bd_addr_t bda;
		memcpy(bda, p_data->srvc_chg.remote_bda, sizeof(esp_bd_addr_t));
		ESP_LOGI(GATTC_TAG, "ESP_GATTC_SRVC_CHG_EVT, bd_addr:");
		esp_log_buffer_hex(GATTC_TAG, bda, sizeof(esp_bd_addr_t));
		esp_log_buffer_hex(GATTC_TAG, gl_profile_tab[PROFILE_A_APP_ID].remote_bda, sizeof(esp_bd_addr_t));
		//==0 moi trung la open lai
		//try connet when dissconnet
		if (memcpy(bda, gl_profile_tab[PROFILE_A_APP_ID].remote_bda,
				sizeof(esp_bd_addr_t)) != 0) {
			uint32_t duration = 5;
			esp_ble_gattc_close(gl_profile_tab[PROFILE_A_APP_ID].gattc_if,
					gl_profile_tab[PROFILE_A_APP_ID].conn_id);
			ESP_LOGI(GATTC_TAG, "ESP_GATTC_SRVC_CHG_EVT, esp_ble_gattc_close");

//			esp_ble_gattc_open(
//											gl_profile_tab[PROFILE_A_APP_ID].gattc_if,
//											gl_profile_tab[PROFILE_A_APP_ID].remote_bda,
//											gl_profile_tab[PROFILE_A_APP_ID].ble_addr_type, true);
			}

		return;

	} else if (event == ESP_GATTC_WRITE_CHAR_EVT) {
		if (p_data->write.status != ESP_GATT_OK) {
			ESP_LOGE(GATTC_TAG, "write char failed, error status = %x",
					p_data->write.status);
			// break;
			return;
		}

		ESP_LOGI(GATTC_TAG, "write char success ");
	} else if (event == ESP_GATTC_DISCONNECT_EVT) {
		connect_bt = false;
		get_server = false;

		if (--cout_shutdown_ble) {
//try connet when timeout
			esp_ble_gap_stop_scanning();
			ESP_LOGI(GATTC_TAG, "SHUTDOWN: %d", cout_shutdown_ble);
			uint32_t duration = 5;
			esp_ble_gap_start_scanning(duration);
		}
//		else {
//			ESP_LOGI(GATTC_TAG, "TIME OUT CONNET,turn off gostickk.", cout_shutdown_ble);
//
//		}
//sleep
		ESP_LOGI(GATTC_TAG, "ESP_GATTC_DISCONNECT_EVT, reason = %d",
				p_data->disconnect.reason);
		return;

	} else if (event == ESP_GATTC_CLOSE_EVT) {
		ESP_LOGI(GATTC_TAG, "ESP_GATTC_CLOSE_EVT, reason = %d",
				p_data->close.reason);
	} else {
		return;

		//to do some thing
	}

}

static void esp_gap_cb(esp_gap_ble_cb_event_t event,
		esp_ble_gap_cb_param_t *param) {
	if(on_ble_server){
		switch (event) {
		#ifdef CONFIG_SET_RAW_ADV_DATA
		    case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT:
		        adv_config_done &= (~adv_config_flag);
		        if (adv_config_done==0){
		            esp_ble_gap_start_advertising(&adv_params);
		        }
		        break;
		    case ESP_GAP_BLE_SCAN_RSP_DATA_RAW_SET_COMPLETE_EVT:
		        adv_config_done &= (~scan_rsp_config_flag);
		        if (adv_config_done==0){
		            esp_ble_gap_start_advertising(&adv_params);
		        }
		        break;
		#else
		    case ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT:
		        adv_config_done &= (~adv_config_flag);
		        if (adv_config_done == 0){
		            esp_ble_gap_start_advertising(&adv_params);
		        }
		        break;
		    case ESP_GAP_BLE_SCAN_RSP_DATA_SET_COMPLETE_EVT:
		        adv_config_done &= (~scan_rsp_config_flag);
		        if (adv_config_done == 0){
		            esp_ble_gap_start_advertising(&adv_params);
		        }
		        break;
		#endif
		    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
		        //advertising start complete event to indicate advertising start successfully or failed
		        if (param->adv_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
		            ESP_LOGE(GATTS_TAG, "Advertising start failed\n");
		        }
		        break;
		    case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
		        if (param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS) {
		            ESP_LOGE(GATTS_TAG, "Advertising stop failed\n");
		        }
		        else {
		            ESP_LOGI(GATTS_TAG, "Stop adv successfully\n");
		        }
		        break;
		    case ESP_GAP_BLE_UPDATE_CONN_PARAMS_EVT:
		         ESP_LOGI(GATTS_TAG, "update connection params status = %d, min_int = %d, max_int = %d,conn_int = %d,latency = %d, timeout = %d",
		                  param->update_conn_params.status,
		                  param->update_conn_params.min_int,
		                  param->update_conn_params.max_int,
		                  param->update_conn_params.conn_int,
		                  param->update_conn_params.latency,
		                  param->update_conn_params.timeout);
		        break;
		    default:
		        break;
		    }
	} else {
	uint8_t *adv_name = NULL;
	uint8_t adv_name_len = 0;
	param->update_conn_params.timeout = 0x0C80;
	switch (event) {
	case ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT: {
		//the unit of the duration is second
		uint32_t duration = 30;
		esp_ble_gap_start_scanning(duration);
		break;
	}
	case ESP_GAP_BLE_SCAN_START_COMPLETE_EVT:
		//scan start complete event to indicate scan start successfully or failed
		if (param->scan_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
			ESP_LOGE(GATTC_TAG, "scan start failed, error status = %x",
					param->scan_start_cmpl.status);
			break;
		}


		esp_log_buffer_hex(GATTC_TAG,param->scan_rst.bda , sizeof(esp_bd_addr_t));

		ESP_LOGI(GATTC_TAG, "scan start success");


		break;
	case ESP_GAP_BLE_SCAN_RESULT_EVT: { //called khi scan ble xong ??
		esp_ble_gap_cb_param_t *scan_result = (esp_ble_gap_cb_param_t *) param;
		switch (scan_result->scan_rst.search_evt) {
		case ESP_GAP_SEARCH_INQ_RES_EVT:
//            esp_log_buffer_hex(GATTC_TAG, scan_result->scan_rst.bda, 6);
//            ESP_LOGI(GATTC_TAG, "searched Adv Data Len %d, Scan Response Len %d", scan_result->scan_rst.adv_data_len, scan_result->scan_rst.scan_rsp_len);
//			ESP_LOGI(GATTC_TAG,"NUM RES: %d", scan_result->scan_rst.num_resps);
			adv_name = esp_ble_resolve_adv_data(scan_result->scan_rst.ble_adv,
					ESP_BLE_AD_TYPE_NAME_CMPL, &adv_name_len);
//            ESP_LOGI(GATTC_TAG, "searched Device Name Len %d", adv_name_len);
//            esp_log_buffer_char(GATTC_TAG, adv_name, adv_name_len);
//            ESP_LOGI(GATTC_TAG, "\n");
			if (adv_name != NULL) {
				if (strlen(remote_device_name) == adv_name_len
						&& strncmp((char *) adv_name, remote_device_name,
								adv_name_len) == 0) {
					ESP_LOGI(GATTC_TAG, "searched device %s\n",
							remote_device_name);
					if (connect_bt == false) {
						connect_bt = true;
						ESP_LOGI(GATTC_TAG, "connect to the remote device.");
						esp_ble_gap_stop_scanning();
						gl_profile_tab[PROFILE_A_APP_ID].ble_addr_type = scan_result->scan_rst.ble_addr_type;
						esp_ble_gattc_open(
								gl_profile_tab[PROFILE_A_APP_ID].gattc_if,
								scan_result->scan_rst.bda,
								scan_result->scan_rst.ble_addr_type, true);
						//xong gap thi gattc_pròile.._handel moi hoat dong
					}
				}
			}
			break;
		case ESP_GAP_SEARCH_INQ_CMPL_EVT:
			break;
		default:
			break;
		}
		break;
	}

	case ESP_GAP_BLE_SCAN_STOP_COMPLETE_EVT:
		if (param->scan_stop_cmpl.status != ESP_BT_STATUS_SUCCESS) {
			ESP_LOGE(GATTC_TAG, "scan stop failed, error status = %x",
					param->scan_stop_cmpl.status);
			break;
		}
		ESP_LOGI(GATTC_TAG, "stop scan successfully")
		;
		break;

	case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
		if (param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS) {
			ESP_LOGE(GATTC_TAG, "adv stop failed, error status = %x",
					param->adv_stop_cmpl.status);
			break;
		}
		ESP_LOGI(GATTC_TAG, "stop adv successfully")
		;
		break;
	case ESP_GAP_BLE_UPDATE_CONN_PARAMS_EVT:
		ESP_LOGI(GATTC_TAG,
				"update connection params status = %d, min_int = %d, max_int = %d,conn_int = %d,latency = %d, timeout = %d",
				param->update_conn_params.status,
				param->update_conn_params.min_int,
				param->update_conn_params.max_int,
				param->update_conn_params.conn_int,
				param->update_conn_params.latency,
				param->update_conn_params.timeout)
		;
		break;
	default:
		break;
	}
}
}
static void esp_gattc_cb(esp_gattc_cb_event_t event, esp_gatt_if_t gattc_if,
		esp_ble_gattc_cb_param_t *param) {
	/* If event is register event, store the gattc_if for each profile */
	if (event == ESP_GATTC_REG_EVT) {
		if (param->reg.status == ESP_GATT_OK) {
			gl_profile_tab[param->reg.app_id].gattc_if = gattc_if;
		} else {
			ESP_LOGI(GATTC_TAG, "reg app failed, app_id %04x, status %d",
					param->reg.app_id, param->reg.status);
			return;
		}
	}

	/* If the gattc_if equal to profile A, call profile A cb handler,
	 * so here call each profile's callback */
	do {
		int idx;
		for (idx = 0; idx < PROFILE_NUM; idx++) {
			if (gattc_if == ESP_GATT_IF_NONE || /* ESP_GATT_IF_NONE, not specify a certain gatt_if, need to call every profile cb function */
			gattc_if == gl_profile_tab[idx].gattc_if) {
				if (gl_profile_tab[idx].gattc_cb) {
					gl_profile_tab[idx].gattc_cb(event, gattc_if, param);
				}
			}
		}
	} while (0);
}

/* resolve a packet from http socket
 * return true if packet including \r\n\r\n that means http packet header finished,start to receive packet body
 * otherwise return false
 * */
bool get_connetion_ble() {
	return connect_bt;
}

static void my_gatts_status_event_cb(const struct event *evt, void *data) {
	/* do stuff and things with the event */
}
bool init_ble_connet() {
//	 Initialize NVS.
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );
	ESP_LOGI(GATTC_TAG, "Begin setup BLE .....");

	ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT));

	esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT()
	;
	ret = esp_bt_controller_init(&bt_cfg);
	if (ret) {
		ESP_LOGE(GATTC_TAG, "%s initialize controller failed: %s\n", __func__,
				esp_err_to_name(ret));
		return false;
	}

	ret = esp_bt_controller_enable(ESP_BT_MODE_BLE);
	if (ret) {
		ESP_LOGE(GATTC_TAG, "%s enable controller failed: %s\n", __func__,
				esp_err_to_name(ret));
		return false;
	}

	ret = esp_bluedroid_init();
	if (ret) {
		ESP_LOGE(GATTC_TAG, "%s init bluetooth failed: %s\n", __func__,
				esp_err_to_name(ret));
		return false;
	}

	ret = esp_bluedroid_enable();
	if (ret) {
		ESP_LOGE(GATTC_TAG, "%s enable bluetooth failed: %s\n", __func__,
				esp_err_to_name(ret));
		return false;
	}



//
//	//register the  callback function to the gap module
//	ret = esp_ble_gap_register_callback(esp_gap_cb);
//	if (ret) {
//		ESP_LOGE(GATTC_TAG, "%s gap register failed, error code = %x\n",
//				__func__, ret);
//		return false;
//	}
if( GATT_SERVER){
	ESP_LOGE("BLE", "CF_BLE_SERVER");
	on_ble_server = true;
	//register the callback function to the gattc module
//	ret = esp_ble_gattc_register_callback(esp_gattc_cb);
//	if (ret) {
//		ESP_LOGE(GATTC_TAG, "%s gattc register failed, error code = %x\n",
//				__func__, ret);
//		return false;
//	}
	 ret = esp_ble_gatts_register_callback(gatts_event_handler);
	    if (ret){
	        ESP_LOGE(GATTS_TAG, "gatts register error, error code = %x", ret);
	        return false;
	    }

		//register the  callback function to the gap module
		ret = esp_ble_gap_register_callback(esp_gap_cb);
		if (ret) {
			ESP_LOGE(GATTC_TAG, "%s gap register failed, error code = %x\n",
					__func__, ret);
			return false;
		}
	ret = esp_ble_gatts_app_register(PROFILE_A_APP_ID_S);
	if (ret) {
		ESP_LOGE(GATTC_TAG, "%s gattc app register failed, error code = %x\n",
				__func__, ret);
	}

	//	//register the  callback function to the gatts module


}else{	    ESP_LOGE("BLE", "CF_BLE_CLIENT");

		//register the  callback function to the gap module
		ret = esp_ble_gap_register_callback(esp_gap_cb);
		if (ret) {
			ESP_LOGE(GATTC_TAG, "%s gap register failed, error code = %x\n",
					__func__, ret);
			return false;
		}
	    //register the callback function to the gattc module
	    	ret = esp_ble_gattc_register_callback(esp_gattc_cb);
	    	if (ret) {
	    		ESP_LOGE(GATTC_TAG, "%s gattc register failed, error code = %x\n",
	    				__func__, ret);
	    		return false;
	    	}

	    	ret = esp_ble_gattc_app_register(PROFILE_A_APP_ID);
	    	if (ret) {
	    		ESP_LOGE(GATTC_TAG, "%s gattc app register failed, error code = %x\n",
	    				__func__, ret);
	    	}

//	    	//	//register the  callback function to the gatts module
//
//	    	 ret = esp_ble_gatts_register_callback(gatts_event_handler);
//	    	    if (ret){
//	    	        ESP_LOGE(GATTS_TAG, "gatts register error, error code = %x", ret);
//	    	        return false;
//	    	    }
		}



	esp_err_t local_mtu_ret = esp_ble_gatt_set_local_mtu(500);
	if (local_mtu_ret) {
		ESP_LOGE(GATTC_TAG, "set local  MTU failed, error code = %x",
				local_mtu_ret);
	}

	esp_gatts_status_cb_register(my_gatts_status_event_cb, NULL);



	return true;
//    xTaskCreate(&begin_trans_ota_ble, "begin_trans_ota_ble", 8192, NULL, 6, NULL);
}
bool reconnet_gatt() {
	return true;
}
void set_shutdown(){
	cout_shutdown_ble = COUT_SHUTDOWN_BLE;
}
bool reconnet_gap(uint8_t NUM_GAP) {
//	esp_ble_gattc_close(gl_profile_tab[PROFILE_APP_ID].gattc_if,gl_profile_tab[PROFILE_APP_ID].conn_id);
	esp_ble_gap_disconnect(gl_profile_tab[NUM_GAP].remote_bda);
	esp_ble_gap_stop_scanning();
	connect_bt = false;
	get_server = false;
	uint32_t duration = 5;
	esp_ble_gap_start_scanning(duration);
	return true;
}
bool send_str(uint8_t* str, uint16_t value_len, uint8_t PROFILE_APP_ID) {
	uint8_t write_char_data[35];
	for (int i = 0; i < sizeof(write_char_data); ++i) {
		write_char_data[i] = 0xAA;
	}
	ESP_LOGI("BLE_SEND", "size: %d , str: %s", sizeof(str), (uint8_t* )str);
	esp_err_t err = esp_ble_gattc_write_char(
			gl_profile_tab[PROFILE_APP_ID].gattc_if,
			gl_profile_tab[PROFILE_APP_ID].conn_id,
			gl_profile_tab[PROFILE_APP_ID].char_handle, value_len,
			(uint8_t*) str, ESP_GATT_WRITE_TYPE_RSP, ESP_GATT_AUTH_REQ_NONE);
	if (err != ESP_OK) {
		return false;
	}
	return true;
}
;
