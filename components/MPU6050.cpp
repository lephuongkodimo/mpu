/**
 * Encapsulate the MPU6050.
 */

#include "MPU6050.h"
#define I2C_ADDRESS 0x68 // I2C address of MPU6050

#define MPU6050_ACCEL_XOUT_H 0x3B
#define MPU6050_GYRO_XOUT_H  0x43
#define MPU6050_PWR_MGMT_1   0x6B
#define INT_ENABLE 		     0x38
#define FIFO_EN				 0x23
#define MPU6050_GYRO_CONFIG  0x1b
#define SIGNAL_PATH_RESET  0x68
#define I2C_SLV0_ADDR      0x37
#define ACCEL_CONFIG       0x1C
#define MOT_THR            0x1F  // Motion detection threshold bits [7:0]
#define MOT_DUR            0x20  // Duration counter threshold for motion interrupt generation, 1 kHz rate, LSB = 1 ms
#define MOT_DETECT_CTRL    0x69
#define INT_ENABLE         0x38
#define WHO_AM_I_MPU6050   0x75 // Should return 0x68

#define RAD_TO_DEG 57.2957786
/**
 * @brief Construct an %MPU6050 handler.
 */
MPU6050::MPU6050() {
	accel_x = accel_y = accel_z = 0;
	gyro_x  = gyro_y  = gyro_z  = 0;
	i2c=nullptr;
	inited=false;
}


/**
 * @brief Destory the class instance.
 */
MPU6050::~MPU6050() {
	delete i2c;
}


/**
 * @brief Read the acceleration value from the device.
 *
 * Calling this method results in communication with the device to retrieve the
 * acceleration data that is then stored in the class instance ready for retrieval.
 */
void MPU6050::readAccel() {
	assert(inited);
	i2c->beginTransaction();
	i2c->write(MPU6050_ACCEL_XOUT_H);
	i2c->endTransaction();

	uint8_t data[6];
	i2c->beginTransaction();
	i2c->read(data, 5, true);
	i2c->read(data+5, false);
	i2c->endTransaction();

	accel_x = (data[0] << 8) | data[1];
	accel_y = (data[2] << 8) | data[3];
	accel_z = (data[4] << 8) | data[5];
//    printf("RAW readAcc!!!\n");
//    printf("X :%d\n ",accel_x);
//    printf("Y :%d\n ",accel_y);
//    printf("Z :%d\n ",accel_z);

	accX = ((float)accel_x) / 16384.0;
	accY = ((float)accel_y) / 16384.0;
	accZ = ((float)accel_z) / 16384.0;

} // readAccel
void MPU6050::calRPY(){

	#ifdef RESTRICT_PITCH // Eq. 25 and 26
	  this->roll  = atan2(accel_y, accel_z) * RAD_TO_DEG;
	  this->pitch = atan(-accel_x / sqrt(accel_y * accel_y + accel_z * accel_z)) * RAD_TO_DEG;
	#else // Eq. 28 and 29
	  this->roll  = atan(accel_y / sqrt(accel_x * accel_x + accel_z * accel_z)) * RAD_TO_DEG;
	  this->pitch = atan2(-accel_x, accel_z) * RAD_TO_DEG;
	#endif
}

/**
 * @brief Read the gyroscopic values from the device.
 *
 * Calling this method results in communication with the device to retrieve the
 * gyroscopic data that is then stored in the class instance ready for retrieval.
 */
void MPU6050::readGyro() {
	assert(inited);
	i2c->beginTransaction();
	i2c->write(MPU6050_GYRO_XOUT_H);
	i2c->endTransaction();

	uint8_t data[6];
	i2c->beginTransaction();
	i2c->read(data, 5, true);
	i2c->read(data+5, false);
	i2c->endTransaction();

	gyro_x = (data[0] << 8) | data[1];
	gyro_y = (data[2] << 8) | data[3];
	gyro_z = (data[4] << 8) | data[5];

//    printf("RAW readGyro!!!\n");
//    printf("X :%d\n ",gyro_x);
//    printf("Y :%d\n ",gyro_y);
//    printf("Z :%d\n ",gyro_z);

	gyroX = ((float)gyro_x) /131.0;
	gyroY = ((float)gyro_y) / 131.0;
	gyroZ = ((float)gyro_z) /131.0;

	gyroX -= gyroXoffset;
	gyroY -= gyroYoffset;
	gyroZ -= gyroZoffset;
} // readGyro

//writeByte
void MPU6050::writeByte(uint8_t subAddress, uint8_t data)
{
	assert(inited);
  	i2c->beginTransaction();
	i2c->write(subAddress);
	i2c->write(data);
	i2c->endTransaction();
//  Serial.println("mnnj");

}

/**
 *dang ki interup cho mpu
 *
 */

void MPU6050::setupInterup() {
		writeByte(  0x6B, 0x00);
	    writeByte(  SIGNAL_PATH_RESET, 0x07); //Reset all internal signal paths in the MPU-6050 by writing 0x07 to register 0x68;
	    writeByte(  I2C_SLV0_ADDR, 0x20); //write register 0x37 to select how to use the interrupt pin. For an active high, push-pull signal that stays until register (decimal) 58 is read, write 0x20.
	    writeByte(  ACCEL_CONFIG, 0x01);//Write register 28 (==0x1C) to set the Digital High Pass Filter, bits 3:0. For example set it to 0x01 for 5Hz. (These 3 bits are grey in the data sheet, but they are used! Leaving them 0 means the filter always outputs 0.)
	    writeByte(  MOT_THR, 40);
	    //Write the desired Motion threshold to register 0x1F (For example, write decimal 20).
	    writeByte(  MOT_DUR, 40 );  //Set motion detect duration to 1  ms; LSB is 1 ms @ 1 kHz rate
	    writeByte(  MOT_DETECT_CTRL, 0x15); //to register 0x69, write the motion detection decrement and a few other settings (for example write 0x15 to set both free-fall and motion decrements to 1 and accelerometer start-up delay to 5ms total by adding 1ms. )
	    writeByte(  INT_ENABLE, 0x40 ); //write register 0x38, bit 6 (0x40), to enable motion detection interrupt.
	    writeByte(  0x37, 0x90 ); // now INT pin is active low in 50us and auto clear bit when have interup other
} // readGyro


/**
 * @brief Initialize the %MPU6050.
 * @param [in] sdaPin The %GPIO pin to use for %I2C SDA.
 * @param [in] clkPin The %GPIO pin to use for %I2C CLK.
 *
 * This method must be called before any other methods.
 */
void MPU6050::init(gpio_num_t sdaPin, gpio_num_t clkPin) {
	i2c = new I2C();
	i2c->init(I2C_ADDRESS, sdaPin, clkPin);
	// Dummy call
	//i2c->setAddress(I2C_ADDRESS);
	i2c->beginTransaction();
	i2c->write(MPU6050_ACCEL_XOUT_H);
	i2c->endTransaction();

	i2c->beginTransaction();
	i2c->write(MPU6050_PWR_MGMT_1);
	i2c->write(0);
	i2c->endTransaction();

	i2c->beginTransaction();
	i2c->write(ACCEL_CONFIG);//Write register 28 (==0x1C) to set the Digital High Pass Filter, bits 3:0. For example set it to 0x01 for 5Hz. (These 3 bits are grey in the data sheet, but they are used! Leaving them 0 means the filter always outputs 0.)
	i2c->write(0x04);
	i2c->endTransaction();

	i2c->beginTransaction();
	i2c->write(MPU6050_GYRO_CONFIG);
	i2c->write(0x00);
	i2c->endTransaction();


	inited=true;

}
void MPU6050::calcGyroOffsets(bool console){
	float x = 0, y = 0, z = 0;
	float ax = 0, ay = 0, az = 0;
	int16_t rx, ry, rz;
	vTaskDelay(2000);
	if(console){
    printf("\n========================================\n");
    printf("calculate gyro offsets\n");
    printf("DO NOT MOVE A MPU6050\n");
	}
	for(int i = 0; i < 3000; i++){
		if(console && i % 1000 == 0){
			Serial.print(".");
		}
		assert(inited);
		i2c->beginTransaction();
		i2c->write(MPU6050_GYRO_XOUT_H);
		i2c->endTransaction();

		uint8_t data[6];
		i2c->beginTransaction();
		i2c->read(data, 5, true);
		i2c->read(data+5, false);
		i2c->endTransaction();

		gyro_x = (data[0] << 8) | data[1];
		gyro_y = (data[2] << 8) | data[3];
		gyro_z = (data[4] << 8) | data[5];

		x += ((float)gyro_x) / 131.0;
		y += ((float)gyro_y) / 131.0;
		z += ((float)gyro_z) / 131.0;
		//acc
		i2c->beginTransaction();
		i2c->write(MPU6050_ACCEL_XOUT_H);
		i2c->endTransaction();

		uint8_t adata[6];
		i2c->beginTransaction();
		i2c->read(adata, 5, true);
		i2c->read(adata+5, false);
		i2c->endTransaction();

		accel_x = (adata[0] << 8) | adata[1];
		accel_y = (adata[2] << 8) | adata[3];
		accel_z = (adata[4] << 8) | adata[5];

		ax += ((float)accel_x) / 16384.0;
		ay += ((float)accel_y) / 16384.0;
		az += ((float)accel_z) / 16384.0;

	}
	gyroXoffset = x / 3000;
	gyroYoffset = y / 3000;
	gyroZoffset = z / 3000;

	accXoffset = ax / 3000;
	accYoffset = ay / 3000;
	accZoffset = az / 3000;
	accZoffset =1 - accZoffset;
	if(console){

    printf("Done!!!\n");
    printf("X :%0.5f\n ",gyroXoffset);
    printf("Y :%0.5f\n ",gyroYoffset);
    printf("Z :%0.5f\n ",gyroZoffset);
    printf("aZ :%0.5f\n ",accZoffset);
    float one = sqrt(accel_x * accel_x + accel_z * accel_z);
    printf("one :%0.5f\n ",one);
    printf("Program will start now\n");
	printf("========================================\n");
	}
}
float MPU6050::getAccZoffset(){
	return accZoffset;
}
