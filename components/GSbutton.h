/*
 * GSbutton.h
 *
 *  Created on: Jul 11, 2018
 *      Author: Kodimo
 */

#ifndef COMPONENTS_GBBLOCKREADERMANAGER_GSBUTTON_H_
#define COMPONENTS_GBBLOCKREADERMANAGER_GSBUTTON_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include <esp_log.h>
//#include <GSdefine.h>

#include "esp_types.h"
#include "soc/timer_group_struct.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"

#define TIMER_DIVIDER         16  //  Hardware timer clock divider
#define TIMER_SCALE           (TIMER_BASE_CLK / TIMER_DIVIDER)  // convert counter value to seconds
#define TIMER_INTERVAL0_SEC   (1.5) // sample test interval for the first timer
#define TIMER_INTERVAL1_SEC   (1)   // sample test interval for the second timer
#define TEST_WITH_RELOAD      1        // testing will be done with auto reload

typedef struct {
    int type;  // the type of timer's event
    int timer_group;
    int timer_idx;
    uint64_t timer_counter_value;
} timer_event_t;

#define GPIO_INPUT_INT GPIO_NUM_25
#define GPIO_INPUT_PIN_SEL  (1ULL<<GPIO_INPUT_INT)
#define ESP_INTR_FLAG_DEFAULT 0
class GSButton;
class GSButtonCallback;

class GSButtonInterup{
public:
	GSButtonInterup(){

	}
	~GSButtonInterup(){

	}

	GSButton* bt;
	void initGSInterup(gpio_isr_t gpio_isr_handler){
		gpio_config_t io_conf;
	    //interrupt of rising edge
	    io_conf.intr_type = GPIO_INTR_LOW_LEVEL;
	    //bit mask of the pins, use GPIO4/5 here
	    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
	    //set as input mode
	    io_conf.mode = GPIO_MODE_INPUT;
	    //enable pull-up mode
	    io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
	    gpio_config(&io_conf);

	    //install gpio isr service
	     gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
	     //hook isr handler for specific gpio pin
	     gpio_isr_handler_add(GPIO_INPUT_INT, gpio_isr_handler, (void*) GPIO_INPUT_INT);
	    //change gpio intrrupt type for one pin
//	     gpio_set_intr_type(GPIO_INPUT_INT, GPIO_INTR_ANYEDGE);

	}
	void initGSInterup(gpio_num_t gpio_num,gpio_isr_t gpio_isr_handler,gpio_int_type_t gpio_intr_type,bool add){
		gpio_config_t io_conf;
	    //interrupt of rising edge
	    io_conf.intr_type = gpio_intr_type;
	    //bit mask of the pins, use GPIO4/5 here
	    io_conf.pin_bit_mask = (1ULL<<gpio_num);
	    //set as input mode
	    io_conf.mode = GPIO_MODE_INPUT;
	    //enable pull-up mode
	    io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
	    gpio_config(&io_conf);

	    //install gpio isr service
	    if(!add)
	     gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
	     //hook isr handler for specific gpio pin
	     gpio_isr_handler_add(gpio_num, gpio_isr_handler, (void*) gpio_num);
	    //change gpio intrrupt type for one pin
//	     gpio_set_intr_type(GPIO_INPUT_INT, GPIO_INTR_ANYEDGE);

	}
	void GSIntrDisable(gpio_num_t gpio_num){
		gpio_intr_disable(gpio_num);
	}
	void GSIntrEnable(gpio_num_t gpio_num){
		gpio_intr_enable(gpio_num);
		}



};
class GSTimer{
public:
	static void tg0_timer_init(timer_idx_t timer_idx,
	     void (*timer_group0_isr)(void*)  )
	{
		const double timer_interval_sec = TIMER_INTERVAL1_SEC;
		bool auto_reload = TEST_WITH_RELOAD;
	    /* Select and initialize basic parameters of the timer */
	    timer_config_t config;
	    config.divider = TIMER_DIVIDER;
	    config.counter_dir = TIMER_COUNT_UP;
	    config.counter_en = TIMER_PAUSE;
	    config.alarm_en = TIMER_ALARM_EN;
	    config.intr_type = TIMER_INTR_LEVEL;
	    config.auto_reload = auto_reload;
	    timer_init(TIMER_GROUP_0, timer_idx, &config);

	    /* Timer's counter will initially start from value below.
	       Also, if auto_reload is set, this value will be automatically reload on alarm */
	    timer_set_counter_value(TIMER_GROUP_0, timer_idx, 0x00000000ULL);

	    /* Configure the alarm value and the interrupt on alarm. */
	    timer_set_alarm_value(TIMER_GROUP_0, timer_idx, timer_interval_sec * TIMER_SCALE);
	    timer_enable_intr(TIMER_GROUP_0, timer_idx);
	    timer_isr_register(TIMER_GROUP_0, timer_idx,  timer_group0_isr,
	        (void *) timer_idx, ESP_INTR_FLAG_IRAM, NULL);
	    timer_start(TIMER_GROUP_0, timer_idx);

	}
	//clear timer and start timer only once
	static void timerStart(timer_idx_t timer_idx){
		TIMERG0.hw_timer[timer_idx].config.alarm_en = 1;
	}



};
class GSButton: public GSTimer {
public:
	GSButton();
	~GSButton();

	bool btPressed;
	bool ButtonPressed();
	void GSbuttonSetCallback(GSButtonCallback* pButtonCallback);
private:
	GSButtonCallback* m_pGSbuttonCallback;
};

class GSButtonCallback {
public:

	virtual void onButonPressed(void *pvParam) = 0;
};

#endif /* COMPONENTS_GBBLOCKREADERMANAGER_GSBUTTON_H_ */
